# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:24:13 2020

@author: egu
"""

from classes.Car import RenaultZOE
from classes.Timer import Timer
from classes.CarPark import CarPark

import matplotlib.pyplot as plt
from datetime import date, time, timedelta
import time as ttime

import numpy as np
import pandas as pd
from random import randint
import shelve
import os

from pyomo.environ import SolverFactory

projectdir=os.path.dirname(os.path.dirname(__file__))
inputsdir =os.path.join(projectdir,'inputs')
pricefile =os.path.join(inputsdir,'elprice')

pricedata = shelve.open(pricefile)
price_series=pricedata['elprice']
pricedata.close() 



def test_case(a1,a2,b1,b2,c1,c2,e1,e2,e3,e4,e5,e6):

    #Specific case inputs price_series
    year = 2020
    month = 1
    day = 6
    timer_start_hour= 12
    timer_start_min = 0
    timer_step_sec  = 60*60 #Size of the steps to be taken at each instant in the simulation (expressed in seconds)
    myTimer = Timer(year, month, day, timer_start_hour, timer_start_min, timer_step_sec)
        
    mmc_cell_nb_per_arm  = 50
    mmc_cell_max_power   = 11
    mmc_system_max_power = 6*mmc_cell_nb_per_arm*mmc_cell_max_power
    myPark = CarPark(myTimer, mmc_cell_nb_per_arm, mmc_system_max_power, mmc_cell_max_power)
        
    myPark.load_price(price_series,timedelta(minutes=5))
           
    sim_start   = myTimer.now                      #Hour when the simulation is starting
    sim_end     = myTimer.now+timedelta(hours=6)   #Hour when the simulation should finish
    sim_step    = myTimer.dT
    #sim_step_min= int(myTimer.dT/timedelta(minutes=1))
    sim_step_no = int((sim_end-sim_start)/sim_step)
    sim_allsteps= [sim_start+t*sim_step for t in range(sim_step_no+1)]
        
    
    #Departure times are assigned to the cars at the moment of arrival
    park_entering_cars=dict([(st,{}) for st in sim_allsteps])   
    park_leaving_cars =dict([(st,[]) for st in sim_allsteps])   
    
    #Specifcy the fleet
    fleet={}
    fleet['ev01']=RenaultZOE(myTimer,'ev01')
    fleet['ev02']=RenaultZOE(myTimer,'ev02')
    fleet['ev03']=RenaultZOE(myTimer,'ev03')
    fleet['ev04']=RenaultZOE(myTimer,'ev04')
    fleet['ev05']=RenaultZOE(myTimer,'ev05')
    fleet['ev06']=RenaultZOE(myTimer,'ev06')
    fleet['ev07']=RenaultZOE(myTimer,'ev07')
    
    fleet['ev01'].soc[sim_start]=a1/100
    fleet['ev02'].soc[sim_start]=a2/100
    fleet['ev03'].soc[sim_start]=b1/100
    fleet['ev04'].soc[sim_start]=b2/100
    fleet['ev05'].soc[sim_start]=c1/100        
    fleet['ev06'].soc[sim_start]=c2/100
    fleet['ev07'].soc[sim_start]=0.2
    
    park_entering_cars[sim_start][fleet['ev01']]={'Estimated departure':sim_end-e1*sim_step,'Desired SoC':1.0}
    park_entering_cars[sim_start][fleet['ev02']]={'Estimated departure':sim_end-e2*sim_step,'Desired SoC':1.0}
    park_entering_cars[sim_start][fleet['ev03']]={'Estimated departure':sim_end-e3*sim_step,'Desired SoC':1.0}
    park_entering_cars[sim_start][fleet['ev04']]={'Estimated departure':sim_end-e4*sim_step,'Desired SoC':1.0}
    park_entering_cars[sim_start][fleet['ev05']]={'Estimated departure':sim_end-e5*sim_step,'Desired SoC':1.0}
    park_entering_cars[sim_start][fleet['ev06']]={'Estimated departure':sim_end-e6*sim_step,'Desired SoC':1.0}
    park_entering_cars[sim_start][fleet['ev07']]={'Estimated departure':sim_end,'Desired SoC':1.0}
    park_leaving_cars[sim_end].append('ev01')
    park_leaving_cars[sim_end].append('ev02')
    park_leaving_cars[sim_end].append('ev03')
    park_leaving_cars[sim_end].append('ev04')
    park_leaving_cars[sim_end].append('ev05')
    park_leaving_cars[sim_end].append('ev06')
    park_leaving_cars[sim_end].append('ev07')
    
            
    optSolver  =SolverFactory("gurobi")
    opt_step   =sim_step
    opt_horizon=timedelta(hours=12)
           
    enteringCars=park_entering_cars[sim_start]
    myPark.enter_cars(sim_start,enteringCars)
    
    
    j1,k1,n1=myPark.simple_allocation(sim_start,opt_step,opt_horizon,fleet['ev01'],optSolver,initialscheduling=0)
    myPark.connect_car(sim_start,fleet['ev01'],j1,k1,n1)
    j2,k2,n2=myPark.simple_allocation(sim_start,opt_step,opt_horizon,fleet['ev02'],optSolver,initialscheduling=0)
    myPark.connect_car(sim_start,fleet['ev02'],j2,k2,n2)
    j3,k3,n3=myPark.simple_allocation(sim_start,opt_step,opt_horizon,fleet['ev03'],optSolver,initialscheduling=0)
    myPark.connect_car(sim_start,fleet['ev03'],j3,k3,n3)
    j4,k4,n4=myPark.simple_allocation(sim_start,opt_step,opt_horizon,fleet['ev04'],optSolver,initialscheduling=0)
    myPark.connect_car(sim_start,fleet['ev04'],j4,k4,n4)
    j5,k5,n5=myPark.simple_allocation(sim_start,opt_step,opt_horizon,fleet['ev05'],optSolver,initialscheduling=0)
    myPark.connect_car(sim_start,fleet['ev05'],j5,k5,n5)
    j6,k6,n6=myPark.simple_allocation(sim_start,opt_step,opt_horizon,fleet['ev06'],optSolver,initialscheduling=0) 
    myPark.connect_car(sim_start,fleet['ev06'],j6,k6,n6)
    
    car=fleet['ev07']
    j,k,n = myPark.optimized_allocation(sim_start,opt_step,opt_horizon,car,optSolver)
    myPark.connect_car(sim_start,car,j,k,n)
    print(j,k,n)
        
    return j,k



np.random.seed(1)
allocation={}



j1,k1=test_case(20,20,20,20,20,20,0,4,0,0,0,0)
allocation[0,4,0,0,0,0]={'phs':j1,'arm':k1}

j2,k2=test_case(20,20,20,20,20,20,4,0,0,0,0,0)
allocation[4,0,0,0,0,0]={'phs':j1,'arm':k1}




#%%
#for rand in range(10):
#    a1,a2,b1,b2,c1,c2=np.random.choice(list(range(20,100,10)),6).tolist()
#    j,k=test_case(a1/100,a2/100,b1/100,b2/100,c1/100,c2/100)
#    allocation[a1,a2,b1,b2,c1,c2]={'phs':j,'arm':k}
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 17:13:45 2020

@author: egu
"""

from classes.Car import RenaultZOE
from classes.Timer import Timer
from classes.CarPark import CarPark

import networkx as nx
import matplotlib.pyplot as plt
from datetime import date, time, timedelta
import time as ttime

import numpy as np
import pandas as pd
from random import randint
import shelve
import os

from pyomo.environ import SolverFactory

def test_case(withopt,curtailed):
    #Scenario paramaters
    nbcars=30   #Number of cars to arrive in the car park in the specified period
    unb=0.05    #Allowed unbalance
    hh=4        #Total hour duration of the simulation 

    #Simulation time parameters (resolution, horizon etc.)
    year = 2020
    month = 1
    day = 6
    timer_start_hour= 12
    timer_start_min = 0
    timer_step_sec  = 60*5 #Size of the steps to be taken at each instant in the simulation (expressed in seconds)
    myTimer = Timer(year, month, day, timer_start_hour, timer_start_min, timer_step_sec)
    
    sim_start   = myTimer.now                       #Hour when the simulation is starting
    sim_end     = myTimer.now+timedelta(hours=hh)   #Hour when the simulation should finish
    sim_step    = myTimer.dT
    sim_step_no = int((sim_end-sim_start)/sim_step)
        
    #Specify the MMC based park parameters
    mmc_cell_nb_per_arm  = 20
    mmc_cell_max_power   = 22
    mmc_system_max_power = 6*mmc_cell_nb_per_arm*mmc_cell_max_power
    myPark = CarPark(myTimer, mmc_cell_nb_per_arm, mmc_system_max_power, mmc_cell_max_power)
    
    #External input for the electricity price series
    projectdir=os.path.dirname(os.path.dirname(__file__))
    inputsdir =os.path.join(projectdir,'inputs')
    pricefile =os.path.join(inputsdir,'elprice')
    pricedata = shelve.open(pricefile)
    price_series=pricedata['elprice']
    pricedata.close()    
    myPark.load_price(price_series,timedelta(minutes=5)) #Loading the electricity price
          

    #Specifcy the arrival scenarios
    np.random.seed(0) #Random seed selection for arrival scenarios
    park_enter_leave=pd.DataFrame(columns=['Object','Arrival','SoC','Departure','Desired SoC'])
      
    free_carList = []
    fleet={}
    for n in range(1,nbcars+1):
        iD='ev'+"{:02d}".format(n)                  
        fleet[iD]=RenaultZOE(myTimer,iD)            #Each car has to be initialized with a timer object and an identifier
        ini_soc    =np.random.randint(20,50)/100
        fin_soc    =1.0
        dod        =fin_soc-ini_soc
        fleet[iD].soc[sim_start]=ini_soc            #State of charge of the cars are assigned at the begining of simulation
        free_carList.append(fleet[iD])              #When a car arrives (leaves) the car park, it will be removed (added) from (in) this list. 
        
        steps2full =int(dod*fleet[iD].bCapacity/(mmc_cell_max_power*sim_step.seconds)) #How many timesteps does it take to charge the battery with given charging power
        
        arrival_step  =np.random.randint(0,sim_step_no-steps2full)                     #A random arrival time for the car   
        departure_step=arrival_step+steps2full                                         #Departure time is calculated by considering the charging duration calculated above  
        park_enter_leave.loc[iD]={'Object':fleet[iD],'Arrival':sim_start+arrival_step*sim_step,
                            'SoC':ini_soc,'Departure':sim_start+departure_step*sim_step,
                            'Desired SoC':fin_soc} #The required elements are added to the park_enter_leave table
        
    #Optimization parameters
    opt_solver  =SolverFactory("gurobi")
    opt_step   =sim_step
    opt_horizon=timedelta(hours=3)
    alpha =mmc_cell_max_power*mmc_cell_nb_per_arm*2*unb #Horizontal unbalance limitation
    beta  =mmc_cell_max_power*mmc_cell_nb_per_arm*1*unb #Vertical unbalance limitation
                    
    #----------------------------------------------- Simulation -----------------------------------------------#
    ############################################################################################################
    for shifted_step in range(sim_step_no+1):
        
        sim_ts=sim_start+shifted_step*sim_step
        #print("Simulation step",sim_ts)
    
        #Loop through the list of cars that leave/arrive the park at this time step
        park_leaving_cars =park_enter_leave[park_enter_leave['Departure']==sim_ts].index
        park_entering_cars=park_enter_leave[park_enter_leave['Arrival']==sim_ts].index
        
        for car_id in park_leaving_cars:            #If the car is among the leaving cars
            car=park_enter_leave['Object'][car_id]  #The car
            myPark.remove_car(sim_ts,car)           #Remove it from the carpark
            free_carList.append(car)                #Add it to the free_carlist
            
        for car_id in park_entering_cars:           #If the car is among the leaving cars
            car=park_enter_leave['Object'][car_id]  #The car
            free_carList.remove(car)                #Remove it from the free_carlist
            enteringCars={}                         
            deptime=park_enter_leave['Departure'][car_id]  #Get the next departure time from the fleet data table
            finsoc =park_enter_leave['Desired SoC'][car_id]#Get the desired SOC after the session from the fleet data table 
            enteringCars[car]={'Estimated departure':deptime,'Desired SoC':finsoc}
            myPark.enter_cars(sim_ts,enteringCars)
            
            if withopt=='simple':
                j,k,n = myPark.simple_allocation(sim_ts,opt_step,opt_horizon,car,opt_solver) #Allocate the car using the simple approach
            if withopt=='optimized':
                j,k,n = myPark.optimized_allocation(sim_ts,opt_step,opt_horizon,car,opt_solver) #Allocate the car using the optimized approach
            myPark.connect_car(sim_ts,car,j,k,n)  #Connect the car to the specified MMC cell

        if curtailed==0:
            pref=myPark.get_cell_references_for_unlimited_unbalance(sim_ts,sim_step)
        if curtailed==1:
            pref=myPark.get_cell_references_for_limited_unbalance(sim_ts,sim_step,alpha,beta)
        myPark.implement_power_references(sim_ts,pref) #Supply the referene powers
            
        #Run idle function for the cars who are not in the MMC park      
        for car in free_carList:
            car.idle(myTimer.now)  
            
        #Save the arm and phase power data of the simulated step    
        myPark.save_power_data(sim_ts)
        
        #Update the timer (shift the simulation step to the next)
        myPark.update_timer()
    ############################################################################################################
        
    armpow,phspow,armcar,phscar=myPark.simulation_summary()
    
    return myPark.host_dataset,armpow,phspow,armcar,phscar,park_enter_leave


#Simulating four cases 
print("Simple allocation non-curtailed")
f00,armpow00,phspow00,armcar00,phscar00,scenario00=test_case('simple',0)
#print("Optimized allocation non-curtailed")
#f10,armpow10,phspow10,armcar10,phscar10,scenario10=test_case('optimized',0)
print("Simple allocation curtailed")
f01,armpow01,phspow01,armcar01,phscar01,scenario01=test_case('simple',1)
#print("Optimized allocation curtailed")
#f11,armpow11,phspow11,armcar11,phscar11,scenario11=test_case('optimized',1)




# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 17:58:12 2020

@author: egu
"""

import numpy as np
import pandas as pd
from datetime import datetime,timedelta




cardata=pd.read_excel('1000cars.xlsx',na_values='Single')
cardata=cardata.fillna('N/A')
cardata['Number of sessions']=np.ones(len(cardata))*2
cardata.loc[cardata[cardata['Connection 2 Start']=='N/A'].index,'Number of sessions']=1


one_session=cardata[cardata['Number of sessions']==1]
two_session=cardata[cardata['Number of sessions']==2]

#%%
#Day chapters
bef_work =datetime(2020,1,3,7,0) 
aft_work=datetime(2020,1,3,17,0)

def dod_in_sec_chr(x,y):
    return (y-x)*22*3600

def sec_to_delta(sec):
    return timedelta(seconds=sec/11)

seconds=dod_in_sec_chr(two_session['Connection 2 Ini SoC'],two_session['Connection 2 Fin SoC'])
tidelta=seconds.apply(lambda x: sec_to_delta(x))


def return_ti_delta(df):
    
    dod   =df['Connection 2 Fin SoC']-df['Connection 2 Ini SoC']
    dod_en=dod*22*3600
    n_sec =dod_en/11   
    tidelt=n_sec.apply(lambda x:timedelta(seconds=int(x)))
    con_end=df['Connection 2 Start']+tidelt
    
    return con_end
    
con_end=return_ti_delta(two_session)
two_session['Connection 2 End']=con_end




"""
#%%
start_one_session_mor      =one_session[one_session['Connection 1 Start']<=bef_work]
start_one_session_noo      =one_session[(one_session['Connection 1 Start']>bef_work)&(one_session['Connection 1 Start']<aft_work)]
start_one_session_eve      =one_session[one_session['Connection 1 Start']>=aft_work]

start_two_session_mor_mor  =two_session[(two_session['Connection 1 Start']<=bef_work)&(two_session['Connection 2 Start']<=bef_work)]
start_two_session_mor_noo  =two_session[(two_session['Connection 1 Start']<=bef_work)&(two_session['Connection 2 Start']>bef_work)&(two_session['Connection 2 Start']<aft_work)]
start_two_session_mor_eve  =two_session[(two_session['Connection 1 Start']<=bef_work)&(two_session['Connection 2 Start']>=aft_work)]
start_two_session_noo_noo  =two_session[(two_session['Connection 1 Start']>bef_work)&(two_session['Connection 1 Start']<aft_work)&(two_session['Connection 2 Start']<aft_work)]
start_two_session_noo_eve  =two_session[(two_session['Connection 1 Start']>bef_work)&(two_session['Connection 1 Start']<aft_work)&(two_session['Connection 2 Start']>=aft_work)]

#%%
one_session_str_mor_end_mor=start_one_session_mor[start_one_session_mor['Connection 1 End']<=bef_work]
one_session_str_mor_end_noo=start_one_session_mor[(start_one_session_mor['Connection 1 Start']>bef_work)&(start_one_session_mor['Connection 1 Start']<aft_work)]
one_session_str_mor_end_eve=start_one_session_mor[start_one_session_mor['Connection 1 Start']>=aft_work]

one_session_str_noo_end_mor=start_one_session_noo[start_one_session_noo['Connection 1 End']<=bef_work]
one_session_str_noo_end_noo=start_one_session_noo[(start_one_session_noo['Connection 1 Start']>bef_work)&(start_one_session_noo['Connection 1 Start']<aft_work)]
one_session_str_noo_end_eve=start_one_session_noo[start_one_session_noo['Connection 1 Start']>=aft_work]

one_session_str_eve_end_mor=start_one_session_eve[start_one_session_eve['Connection 1 End']<=bef_work]
one_session_str_eve_end_noo=start_one_session_eve[(start_one_session_eve['Connection 1 Start']>bef_work)&(start_one_session_eve['Connection 1 Start']<aft_work)]
one_session_str_eve_end_eve=start_one_session_eve[start_one_session_eve['Connection 1 Start']>=aft_work]

#%%
"""



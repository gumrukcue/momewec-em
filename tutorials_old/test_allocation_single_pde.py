# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 17:05:47 2019

@author: egu
"""


from pyomo.environ import SolverFactory
from datetime import date, datetime, timedelta
import pandas as pd
import numpy as np

from optimizationmodels.deterministic.new_models.allocation_peakminimize import choose_an_arm

def test_algorithm(solver,optMin,optHorizon,w0,w1,mmc_armsize,mmc_p_cell,mmc_loading,ini_conect,sce_list):
    """
    :param optSolver:   SolverFactory object 
    :param optMin:      Optimization time step size (number of minutes in one step)
    :param optHorizon:  Optimization horizon (number of hours to be considered)
    :param w0:          Weight to penalize horizontal imbalance
    :param w1:          Weight to penalize vertical imbalance
    :param mmc_armsize: Number of cells in one arm of MMC
    :param mmc_p_cell:  Nominal charging power to WCU
    :param mmc_loading: Loading scenario is a dictionary whose keys stand for one arm, values are arrays for loading
    :param ini_connect: Number of connected cars in arm
    :param sce_list:    List of different arrival scenarios:  Each scenario is a dictionary for
                                                               battery cap
                                                               arrival time, arrival soc, 
                                                               departure time, desired dep soc) 
    """
    
    #allocation_record= pd.DataFrame([(p,a) for p in [1,2,3] for a in [1,2]])
       
    deltaSec=60*optMin                  #Size of time step in seconds
    horizon =int(optHorizon*60/optMin)  #Optimization horizon is 12 hours
    
    for scenario in sce_list:
        
        print("The scenario")       
        carBCap=scenario['Battery Capacity']
        carISoC=scenario['Arrival SOC']
        carFSoC=scenario['Desired SOC']
        carDep =scenario['Departure Time']
        print('Battery Capacity',carBCap)
        print('Arrival SOC',carISoC)
        print('Desired SOC',carFSoC)
        print('Departure Time',carDep)
        
        phase,arm=choose_an_arm(solver,deltaSec,horizon,w0,w1,mmc_armsize,mmc_p_cell,carBCap,carISoC,carFSoC,carDep,mmc_loading,ini_conect)  
        print("Allocation to:",phase,arm)

###############################################################################
optSolver =SolverFactory("gurobi")
optMin    =5                            #One step is 5 minute
optHor    =6                            #Horizon is 6 hours     
optW0     =1                            #Weight of horizontal imbalance
optW1     =1                            #Weight of vertical imbalance

armSize   =12                           #12 submodule at each arm
cellPNom  =5                            #5kW cell nominal power

simStart=datetime(2019,11,12,16)        #Simulation starts at 4pm today
simEnd  =datetime(2019,11,13,7)         #Simulation finishes at 7am tomorrow
simdT   =timedelta(minutes=optMin)      #Simulation time step size
simDur  =int((simEnd-simStart)/simdT)   #Number of time steps until 7am tomorrow

Scenario1={}
Scenario1['Battery Capacity']=50*3600   #Car 1's battery capacity is 50kWh 
Scenario1['Arrival SOC']=0.2            #Car 1's initial SOC is  20%
Scenario1['Desired SOC']=1.0            #Car 1's desired SOC is 100%
Scenario1['Departure Time']=simDur      #Car 1's next departure
scenarios=[Scenario1]

#Loading scenarios before new car arrives
mmcloading={}
iniconnect={}
for p in [1,2,3]:
    for a in [1,2]:
        mmcloading[p,a]=np.concatenate((np.ones(int(optHor*60/2/optMin)),np.zeros(int(optHor*60/2/optMin))))
        iniconnect[p,a]=1
             
test_algorithm(optSolver,optMin,optHor,optW0,optW1,armSize,cellPNom,mmcloading,iniconnect,scenarios)
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 12:30:19 2019

@author: egu
"""

import time
import pandas as pd
from pyomo.environ import SolverFactory
from communication.server_scheduling import SchedulingServer

optSolver =SolverFactory("cplex")

host='127.0.0.1'
port=22128

server=SchedulingServer(host,port)
print("Initializing the server at adress %s:%s"%(host,port))
server.listen(0)
try:
    server.accept()
except:
    print("No client is connected")
finally:
    print()

try:
    server.receive_request()
    server.optimize_schedules(optSolver)
    optimized=True
except:
    pass
finally:
    server.socket.close()
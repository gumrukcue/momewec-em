# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 12:36:02 2019

@author: egu
"""

from datetime import datetime, timedelta
from communication.client_scheduling import SchedulingClient
from datahandling.report import Report
import numpy as np


armSize     = 12  # 12 submodule at each arm
cellPNom    = 5   # 5kW cell nominal power
cellEff     = 1.0 #Efficiency
mmcpPower   = armSize*cellPNom*6

optMin      = 5  # 5 minute
optTimeSec  = 60 * optMin  # Size of time step in seconds
optHorizon  = int(1 * 60 / optMin)  # Optimization horizon is 1 hours
opt0tol     = 40  # Weight of horizontal imbalance
opt1tol     = 20  # Weight of vertical imbalance

simStart    = datetime(2019, 11, 12, 16)  # Simulation starts at 4pm today

#Connection data set
stdcar={'P_Max':5,'BCap':216000,'minSoC':0.2,'maxSoC':1.0}
arms=[(p,a) for p in [1,2,3] for a in [1,2]]
connectiondict=dict.fromkeys(arms)
car001=stdcar.copy()
car002=stdcar.copy()
car003=stdcar.copy()
car004=stdcar.copy()
car005=stdcar.copy()
car006=stdcar.copy()
car007=stdcar.copy()
car008=stdcar.copy()
car009=stdcar.copy()
car010=stdcar.copy()
car011=stdcar.copy()
car012=stdcar.copy()
car001['SoC']=np.random.uniform(low=0.4,high=0.8)
car002['SoC']=np.random.uniform(low=0.4,high=0.8)
car003['SoC']=np.random.uniform(low=0.4,high=0.8)
car004['SoC']=np.random.uniform(low=0.4,high=0.8)
car005['SoC']=np.random.uniform(low=0.4,high=0.8)
car006['SoC']=np.random.uniform(low=0.4,high=0.8)
car007['SoC']=np.random.uniform(low=0.4,high=0.8)
car008['SoC']=np.random.uniform(low=0.4,high=0.8)
car009['SoC']=np.random.uniform(low=0.4,high=0.8)
car010['SoC']=np.random.uniform(low=0.4,high=0.8)
car011['SoC']=np.random.uniform(low=0.4,high=0.8)
car012['SoC']=np.random.uniform(low=0.4,high=0.8)
car001['Dep'] =np.random.randint(15)
car002['Dep'] =np.random.randint(15)
car003['Dep'] =np.random.randint(15)
car004['Dep'] =np.random.randint(15)
car005['Dep'] =np.random.randint(15)
car006['Dep'] =np.random.randint(15)
car007['Dep'] =np.random.randint(15)
car008['Dep'] =np.random.randint(15)
car009['Dep'] =np.random.randint(15)
car010['Dep'] =np.random.randint(15)
car011['Dep'] =np.random.randint(15)
car012['Dep'] =np.random.randint(15)
car001['Phase']=1
car002['Phase']=1
car003['Phase']=2
car004['Phase']=2
car005['Phase']=3
car006['Phase']=3
car007['Phase']=1
car008['Phase']=1
car009['Phase']=2
car010['Phase']=2
car011['Phase']=3
car012['Phase']=3
car001['Arm']=1
car002['Arm']=2
car003['Arm']=1
car004['Arm']=2
car005['Arm']=1
car006['Arm']=2
car007['Arm']=1
car008['Arm']=2
car009['Arm']=1
car010['Arm']=2
car011['Arm']=1
car012['Arm']=2
connectiondict={'Car001':car001,'Car002':car002,'Car003':car003,'Car004':car004,'Car005':car005,'Car006':car006,
                'Car007':car007,'Car008':car008,'Car009':car009,'Car010':car010,'Car011':car011,'Car012':car012}


#Scheduling client
schclient=SchedulingClient(armSize,cellPNom,cellEff,mmcpPower)

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 22128
schclient.connect_to_server(HOST, PORT)

schclient.pack_data(optTimeSec,optHorizon,opt0tol,opt1tol,connectiondict,simStart)
schclient.send_optimization_request_for_scheduling()
resultdf=schclient.receive_optimization_results_for_scheduling()
print("Charging schedules")
print(resultdf)


# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 13:21:27 2019

@author: egu
"""

from pyomo.environ import SolverFactory
import pandas as pd
import numpy as np
import shelve
from datetime import timedelta,time
import matplotlib.pyplot as plt

from optimizationmodels.deterministic.new_models.scheduling import optimize_charge_schedules
from optimizationmodels.deterministic.new_models.scheduling_no_bill import optimize_charge_schedules_no_bill

def test_case(P_cell,cap,inisoc,hour,considerbill,pricedata,unb):
    ts         =price_series.index[hour]     #00:00+hour
    print(ts)
    optStepSize=timedelta(seconds=3600)
    optHorizon =timedelta(hours=12)
    alpha=unb
    beta =unb
    solver= SolverFactory("gurobi")
    multiplier=10
    P_cell=11

    timerange=[ts+t*optStepSize for t in range(int(optHorizon/optStepSize))]   


    parkData={}
    parkData['Slots']               =list(range(1,51))
    parkData['OptimizationHorizon'] =int(optHorizon/optStepSize)
    parkData['TimeInterval']        =optStepSize.seconds
    parkData['P_MMC_Max']           =P_cell*50*6
    parkData['alpha_p']             =alpha
    parkData['beta_p']              =beta
    parkData['Charging Efficiency'] =1.0
    parkData['Price']               =price_series[timerange].values


    stdcar={'P_Max' :11,
            'BCap'  :cap*3600,
            'minSoC':0.0,
            'maxSoC':1.0,
            'Dep'   :8,
            'SoC'   :inisoc,
            'desSoC':1.0}
    
    #Add 12 car dictionaries
    car001=stdcar.copy()
    car002=stdcar.copy()
    car003=stdcar.copy()
    car004=stdcar.copy()
    car005=stdcar.copy()
    car006=stdcar.copy()

    car001['Phase']=1
    car002['Phase']=1
    car003['Phase']=2
    car004['Phase']=2
    car005['Phase']=3
    car006['Phase']=3

    
    car001['Arm']  =1
    car002['Arm']  =2
    car003['Arm']  =1
    car004['Arm']  =2
    car005['Arm']  =1
    car006['Arm']  =2



    connectiondict={'Car001':car001,'Car002':car002,'Car003':car003,'Car004':car004,'Car005':car005,'Car006':car006}
   
    if considerbill==True:
        result_car_in=optimize_charge_schedules(parkData,connectiondict,solver,multiplier)
    else:
        result_car_in=optimize_charge_schedules_no_bill(parkData,connectiondict,solver,multiplier)
    
    optimization_results_p_car =pd.DataFrame(data=result_car_in)
    
    return optimization_results_p_car


pricedata = shelve.open('elprice')
price_series=pricedata['elprice']
pricedata.close()

pcell =11
bcap  =55
inisoc=0.2

without_bill=test_case(pcell,bcap,inisoc,0,False,price_series,0.00)
schedules=dict.fromkeys(['Car001','Car002','Car003','Car004','Car005','Car006'])
socs=dict.fromkeys(['Car001','Car002','Car003','Car004','Car005','Car006'])
for car in schedules.keys():
    schedules[car]=pd.DataFrame()
    socs[car]=pd.DataFrame()
    schedules[car]['No Price']=without_bill[car]
    socs[car]['No Price']=(without_bill[car].cumsum()+inisoc*bcap)/bcap*100
    
for hour in range(0,18,6):
    start=hour*12
    result=test_case(pcell,bcap,inisoc,start,True,price_series,0.00) 
    for car in schedules.keys():
        result_car=result[car]
        schedules[car][price_series.index[start].strftime("%H:%M")]=result_car
        socs[car][price_series.index[start].strftime("%H:%M")]=(result_car.cumsum()+inisoc*bcap)/bcap*100
                
#%%
fig, ax0 = plt.subplots()
socs['Car001'].plot(ax=ax0)
ax0.set_ylabel('SoC %')
ax0.set_xlabel('Hours after optimization')
plt.show()

#%%
fig1,ax1= plt.subplots()
price_series.iloc[0*12  :24*12].plot(ax=ax1)
ax1.set_ylabel('Eur/MWh')
ax1.set_xlabel('Time of day')
plt.show()
    

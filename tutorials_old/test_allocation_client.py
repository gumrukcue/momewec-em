# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 08:34:36 2019

@author: egu
"""

from datetime import datetime, timedelta
from communication.client_allocation import AllocationClient
from datahandling.report import Report
import numpy as np

#def test_client_server(initial_loading,car_dict):

#Simulation set-up
simStart    = datetime(2019, 11, 12, 16)  # Simulation starts at 4pm today
simEnd      = datetime(2019, 11, 13, 7)   # Simulation finishes at 7am tomorrow
simdT       = timedelta(seconds=300)      # Simulation time step size
simDur      = int((simEnd - simStart) / simdT)  # Number of time steps until 7am tomorrow

report = Report(simStart,12)

optMin      = 5  # 5 minute
optTimeSec  = 60 * optMin  # Size of time step in seconds
optHorizon  = int(1 * 60 / optMin)  # Optimization horizon is 1 hours
optW0       = 1  # Weight of horizontal imbalance
optW1       = 1  # Weight of vertical imbalance

armSize     = 12  # 12 submodule at each arm
cellPNom    = 5  # 5kW cell nominal power

#Initial loading
zeros=np.zeros(12)
tempCPlan = {}
actAlloc  = {}
for p in [1,2,3]:
    for a in [1,2]:
        tempCPlan[p, a] = zeros
        actAlloc[p, a] = 0

#Car arrival scenario
carBCap = 50 * 3600  # Each car's battery capacity is 50kWh
carISoC = 0.2  # Each car's initial SOC is  20%
carFSoC = 1.0  # Each car's desired SOC is 100%
carDep  = simDur

#Allocation client
allocclient = AllocationClient(armSize, cellPNom)

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 22128
allocclient.connect_to_server(HOST, PORT)

#while True:
allocclient.pack_data(optTimeSec, optHorizon, optW0, optW1, carBCap, carISoC, carFSoC, carDep, tempCPlan,actAlloc,simStart)
allocclient.send_optimization_request_for_allocation()
phase, arm = allocclient.receive_optimization_results_for_allocation()

allocclient.client.close()

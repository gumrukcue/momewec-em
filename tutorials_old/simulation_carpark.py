# -*- coding: utf-8 -*-
"""
Created on Wed May 29 10:12:23 2019

@author: egu
"""

import pandas as pd
from itertools import product
from datetime import date, datetime, timedelta
#from pyomo.environ import SolverFactory


from classes.Timer import Timer
from classes.Car import RenaultZOE
from classes.Charger import ChargingModule
from classes.CarPark import CarPark


dep_time1=datetime(2019,4,24,6)    
dep_time2=datetime(2019,4,24,8)    
dep_time3=datetime(2019,4,24,7)    
dep_time4=datetime(2019,4,24,9)    
dep_time5=datetime(2019,4,24,9)    
dep_time6=datetime(2019,4,24,10)

#solver= SolverFactory("gurobi")
mytimer=Timer(2019,4,23,16,0,3600,24)  


#16:00
print("Initial time",mytimer.now)
  
mycar1=RenaultZOE(mytimer,'Car0001',2.5,10)
mycar1.soc[mytimer.now]=0.50
mycar2=RenaultZOE(mytimer,'Car0002',2.5,10)
mycar2.soc[mytimer.now]=0.45
mycar3=RenaultZOE(mytimer,'Car0003',2.5,10)
mycar3.soc[mytimer.now]=0.48
mycar4=RenaultZOE(mytimer,'Car0004',2.5,10)
mycar4.soc[mytimer.now]=0.39
mycar5=RenaultZOE(mytimer,'Car0005',2.5,10)
mycar5.soc[mytimer.now]=0.41
mycar6=RenaultZOE(mytimer,'Car0006',2.5,10)
mycar6.soc[mytimer.now]=0.29
   
mypark=CarPark(mytimer,5,600,20)
#####################################

#17:00
mypark.update_timer()
mycar1.retrieve_soc_data(mytimer.now)
mycar2.retrieve_soc_data(mytimer.now)
mycar3.retrieve_soc_data(mytimer.now)
mycar4.retrieve_soc_data(mytimer.now)
mycar5.retrieve_soc_data(mytimer.now)
mycar6.retrieve_soc_data(mytimer.now)

enteringcars={1:{'car':mycar1,'dep':dep_time1}}
mypark.enter_cars(mytimer.now,enteringcars)
#####################################

#18:00
mypark.update_timer()
mycar1.retrieve_soc_data(mytimer.now)
mycar2.retrieve_soc_data(mytimer.now)
mycar3.retrieve_soc_data(mytimer.now)
mycar4.retrieve_soc_data(mytimer.now)
mycar5.retrieve_soc_data(mytimer.now)
mycar6.retrieve_soc_data(mytimer.now)

enteringcars={1:{'car':mycar2,'dep':dep_time2},2:{'car':mycar3,'dep':dep_time3}}
mypark.enter_cars(mytimer.now,enteringcars)
#####################################

#19:00
mypark.update_timer()
mycar4.retrieve_soc_data(mytimer.now)
mycar5.retrieve_soc_data(mytimer.now)
mycar6.retrieve_soc_data(mytimer.now)

enteringcars={1:{'car':mycar4,'dep':dep_time4},2:{'car':mycar5,'dep':dep_time5}}
mypark.enter_cars(mytimer.now,enteringcars)
#####################################

#20:00
mypark.update_timer()
mycar6.retrieve_soc_data(mytimer.now)

enteringcars={1:{'car':mycar6,'dep':dep_time6}}
mypark.enter_cars(mytimer.now,enteringcars)
#####################################


#21:00
mypark.update_timer()

mypark.connect_car(mytimer.now,mycar1,1,1,1)
mypark.connect_car(mytimer.now,mycar2,1,2,1)
mypark.connect_car(mytimer.now,mycar3,2,1,1)
mypark.connect_car(mytimer.now,mycar4,2,2,1)
mypark.connect_car(mytimer.now,mycar5,3,1,1)
mypark.connect_car(mytimer.now,mycar6,3,2,1)


#####################################
datalst=[mypark]  
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 08:31:19 2019

@author: egu
"""
import socket
from pyomo.environ import SolverFactory
from communication.server_allocation import AllocationServer

optSolver =SolverFactory("cplex")

# Initializing server
host = '127.0.0.1'   # get the hostname, usually -> host='127.0.0.1'
port = 22128
server = AllocationServer(host,port)
print("Initializing the server at adress %s:%s \n"%(host,port))

# Starting to listen for connections
clients_number = 1
# configure how many clients the server can listen simultaneously, controlling 
# the number of incoming connections that are kept "waiting"
server.listen(clients_number)

# Try to accept connections. If it has passed long time without new connections,
# close and restart the server.
try:
    while True:
        server.accept()
        # If client has connected, receive data and send answer back until client
        # closes communication with server.
        while True:
            value = server.receive_request()
            if value == 0:
                # If client has closes socket, close socket on server side and 
                # restart process of hearing new connections.
                server.client.close()
#                server.client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                #print("Connection with client socket closed")
                print()
                break
            # If everything working as expected, optimize request.
            server.optimize_allocation(optSolver)
except socket.timeout:
    server.socket.close()
finally:
    print("Session ended")

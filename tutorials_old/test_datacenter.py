# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 16:22:15 2019

@author: egu
"""

from datetime import datetime, timedelta
from datahandling.mmcdatacenter import DataCenter
import numpy as np

#def test_client_server(initial_loading,car_dict):

#Simulation set-up
simStart    = datetime(2019, 11, 12, 16)  # Simulation starts at 4pm today
simEnd      = datetime(2019, 11, 13, 7)   # Simulation finishes at 7am tomorrow

optMin      = 15  # 5 minute
optTimeSec  = 60 * optMin  # Size of time step in seconds
optHorizon  = int(12 * 60 / optMin)  # Optimization horizon is 1 hours
optW0       = 1  # Weight of horizontal imbalance
optW1       = 1  # Weight of vertical imbalance
opt0tol     = 40  # Weight of horizontal imbalance
opt1tol     = 20  # Weight of vertical imbalance

armsize     = 12  # 12 submodule at each arm
cellpower   = 5  # 5kW cell nominal power
celleff     = 1.0 #Efficiency
mmcpower   = armsize*cellpower*6

mmcdc=DataCenter(simStart,armsize,cellpower,celleff,mmcpower)
mmcdc.set_optimization_parameters(optTimeSec,optHorizon,optW0,optW1,opt0tol,opt1tol)

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 22128
mmcdc.connect_to_server_for_allocation(HOST, PORT)


#Allocation of a new car
#Set up for new car 
zeros=np.zeros(optHorizon)
tempCPlan = {}
for p in [1,2,3]:
    for a in [1,2]:
        tempCPlan[p, a] = zeros
mmcdc.save_temp_charging_schedules(tempCPlan)
carBCap = 50   # Each car's battery capacity is 50kWh
carISoC = 0.2  # Each car's initial SOC is  20%
carFSoC = 1.0  # Each car's desired SOC is 100%
carDep  = simEnd

fast_plan=mmcdc.calculate_fastest_schedule_for_new_car(simStart,carBCap,carISoC,carFSoC,carDep)
phs,arm  =mmcdc.choose_adress_for_new_car(simStart,carBCap,carISoC,carFSoC,carDep)
mmcdc.add_car_to_arm(simStart,phs,arm)






# -*- coding: utf-8 -*-
"""
Created on Fri May 15 10:11:17 2020

@author: egu
"""

from datetime import datetime,timedelta
import numpy as np
import pandas as pd

def find_latest_schedule(ts,stepsize,p,ecap,inisoc,leavets,finsoc=1.0):
    """
    ts      : current time                      datetime.datetime
    stepsize: size of one time step             datetime.timedelta
    p       : nominal charging power     (kW)   float
    ecap    : energy capacity of battery (kWs)  float   
    inisoc  : initial soc (0<inisoc<1)          float
    finsoc  : final soc   (0<inisoc<1)          float
    leavets : estimated leave time              datetime.datetime
    """  
    dt=stepsize.seconds
    
    dod              =finsoc-inisoc                                             #Depth of discharge
    energy_to_fill   =dod*ecap                                                  #Energy needed to fill the battery kWs
    
    steps_to_fill    =energy_to_fill/(p*dt)                                     #How long would it take to fill the battery with this charger
    int_steps_to_fill=int(steps_to_fill)                                        #Number of the steps along which the battery to be charged with full power
    
    lst_step_modulate=steps_to_fill-int_steps_to_fill                           #Modulation rate for the final step before reaching final SOC
    
    leave_ts=leavets
    
    duration=pd.date_range(start=ts,end=leave_ts-stepsize,freq=stepsize)                    #Date range for the whole stay duration in the charging park
    fullpowd=pd.date_range(end=leave_ts-stepsize,freq=stepsize,periods=int_steps_to_fill)   #Date range to specify the steps to charge the batttery with full power
    redpowst=leave_ts-stepsize*(int_steps_to_fill+1)                                        #Time step to charge the batter with modulated power  

    profile=pd.Series(np.zeros(len(duration)),index=duration)
    profile[fullpowd]=p
    profile[redpowst]=p*lst_step_modulate
    
    schedule=profile[duration]
    deltasoc=(schedule.cumsum()*dt)/ecap
    soc     =inisoc+deltasoc.shift(1).fillna(0.0)
    
    return schedule,soc
     
if __name__ == "__main__":
      
    now=datetime(2020,5,15,8)
    dT =timedelta(hours=1)
    P  =10
    E  =50*3600
    ini_soc=0.2
    fin_soc=1.0
    leave  =datetime(2020,5,15,14)
      
    profile,soc=find_latest_schedule(now,dT,P,E,ini_soc,leave,fin_soc)
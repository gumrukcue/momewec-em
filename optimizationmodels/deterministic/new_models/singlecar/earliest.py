# -*- coding: utf-8 -*-
"""
Created on Fri May 15 08:32:29 2020

@author: egu
"""

from datetime import datetime,timedelta
import numpy as np
import pandas as pd

def find_earliest_schedule(ts,stepsize,p,ecap,inisoc,finsoc=1.0,leavets=None):
    """
    ts      : current time                      datetime.datetime
    stepsize: size of one time step             datetime.timedelta
    p       : nominal charging power     (kW)   float
    ecap    : energy capacity of battery (kWs)  float   
    inisoc  : initial soc (0<inisoc<1)          float
    finsoc  : final soc   (0<inisoc<1)          float
    leavets : estimated leave time              datetime.datetime
    """  
    dt=stepsize.seconds
    
    if leavets!=None:
        secondstoleave=(leavets-ts).seconds
        hyp_fin_soc=inisoc+p*secondstoleave/ecap
        fea_fin_soc=min(hyp_fin_soc,finsoc)                                     #Feasible final soc that can be reached in the stay duration
    else:
        fea_fin_soc=finsoc
     
    dod              =fea_fin_soc-inisoc                                        #Depth of discharge
    energy_to_fill   =dod*ecap                                                  #Energy needed to fill the battery kWs
    
    steps_to_fill    =energy_to_fill/(p*dt)                                     #How long would it take to fill the battery with this charger
    int_steps_to_fill=int(steps_to_fill)                                        #Number of the steps along which the battery to be charged with full power
    
    lst_step_modulate=steps_to_fill-int_steps_to_fill                           #Modulation rate for the final step before reaching final SOC
    
    if leavets==None:
        leave_ts=ts+(int_steps_to_fill+2)*stepsize
    else:
        leave_ts=leavets
    
    duration=pd.date_range(start=ts,end=leave_ts,freq=stepsize)        #Date range for the whole stay duration in the charging park
    fullpowd=pd.date_range(start=ts,freq=stepsize,periods=int_steps_to_fill)    #Date range to specify the steps to charge the batttery with full power
    redpowst=ts+stepsize*int_steps_to_fill                                      #Time step to charge the batter with modulated power  


    profile=pd.Series(np.zeros(len(duration)),index=duration)
    profile[fullpowd]=p
    profile[redpowst]=p*lst_step_modulate
    
    schedule=profile[duration]
    deltasoc=(schedule.cumsum()*dt)/ecap
    soc     =inisoc+deltasoc.shift(1).fillna(0.0)
    
    return schedule,soc
     
if __name__ == "__main__":
        
    now=datetime(2020,5,15,8)
    dT =timedelta(hours=1)
    P  =10
    E  =50*3600
    ini_soc=0.3
    fin_soc=1.0
    leave  =datetime(2020,5,15,12)
      
    schedule,soc=find_earliest_schedule(now,dT,P,E,ini_soc,fin_soc,leave)
# -*- coding: utf-8 -*-
"""
Created on Fri May 15 10:24:18 2020

@author: egu
"""

from datetime import datetime,timedelta
import numpy as np
import pandas as pd
import time

from pyomo.environ import SolverFactory
from pyomo.core import *


def find_cheapest_schedule(solver,ts,stepsize,p,ecap,inisoc,price,leavets,finsoc=1.0):
    """
    ts      : current time                      datetime.datetime
    stepsize: size of one time step             datetime.timedelta
    p       : nominal charging power     (kW)   float
    ecap    : energy capacity of battery (kWs)  float   
    inisoc  : initial soc (0<inisoc<1)          float
    finsoc  : final soc   (0<inisoc<1)          float
    leavets : estimated leave time              datetime.datetime
    price   : price signal (Eur/MWh)            pandas series
    """  
    
    leave_ts=leavets
    
    duration=pd.date_range(start=ts,end=leave_ts,freq=stepsize)       #Date range for the whole stay duration in the charging park
    priceseries=price.reindex(duration)
    priceseries=priceseries.fillna(method='ffill')/1000
    
    secondstoleave=(leave_ts-ts).seconds
    hyp_fin_soc=inisoc+p*secondstoleave/ecap
    fea_fin_soc=min(hyp_fin_soc,finsoc)                 #Feasible final soc that can be reached in the stay duration

    ####################Constructing the optimization model####################
    model       = ConcreteModel()
    
    model.T     = Set(initialize=duration,ordered=True)  #Time index set
    
    model.dt    = stepsize.seconds          #Step size
    model.E     = ecap                      #Battery capacity in kWs
    model.Pmax  = p                         #Maximum charging power in kW
    model.price = priceseries               #Energy price series
    model.SoC_F = fea_fin_soc               #SoC to be achieved at the end
    
    model.p     = Var(model.T,within=NonNegativeReals,bounds=(0,model.Pmax))
    model.SoC   = Var(model.T,within=NonNegativeReals,bounds=(0,1))
    
    #CONSTRAINTS
    def initialsoc(model):
        return model.SoC[min(duration)]==inisoc
    model.inisoc=Constraint(rule=initialsoc)
            
    def storageConservation(model,t):#SOC of EV batteries will change with respect to the charged power and battery energy capacity
        if t<max(duration):
            return model.SoC[t+stepsize]==(model.SoC[t] + model.p[t]*model.dt/model.E)
        else:
            return model.SoC[t] ==model.SoC_F
    model.socconst=Constraint(model.T,rule=storageConservation)
    
    def supplyrule(model):
        return model.p[max(duration)]==0.0
    model.supconst=Constraint(rule=supplyrule)
       
    #OBJECTIVE FUNCTION
    def obj_rule(model):  
        return sum(model.p[t]*model.dt*model.price[t] for t in model.T)
    model.obj=Objective(rule=obj_rule, sense = minimize)
    
    solver.solve(model)
    
    sch_dict={}
    soc_dict={}
    
    for t in model.T:
        sch_dict[t]=model.p[t]()
        soc_dict[t]=model.SoC[t]()
    soc_dict[leave_ts]=fea_fin_soc
        
    schedule=pd.Series(sch_dict)
    soc     =pd.Series(soc_dict)

    return schedule,soc
     
if __name__ == "__main__":
        
    now=datetime(2020,5,15,8)
    dT =timedelta(minutes=15)
    P  =10
    E  =50*3600
    ini_soc=0.2
    fin_soc=0.8
    leave  =datetime(2020,5,15,14)
    
    
    pr_st=datetime(2020,5,15)
    pr_en=datetime(2020,5,16)
    prind=pd.date_range(start=pr_st,end=pr_en,freq=timedelta(hours=1))
    elprice=pd.Series(np.ones(len(prind)),index=prind)
    elprice[pd.date_range(start=pr_st+3*timedelta(hours=3),freq=timedelta(hours=1),periods=10)]=2
    
    optsolver=SolverFactory("gurobi")
    
    trial=[]
    for i in range(100):
    
        start=time.time()
        schedule,soc=find_cheapest_schedule(optsolver,now,dT,P,E,ini_soc,elprice,leave,fin_soc)
        end=time.time()
        exetim=end-start
        trial.append(exetim)
        
    print(sum(trial)/100)
        
        
        
        
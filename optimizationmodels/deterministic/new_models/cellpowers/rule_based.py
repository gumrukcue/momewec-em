# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 13:34:57 2020

@author: egu
"""

def intervene_unbalance_for_dummy_charging(ts,optStepSize,armcells,chargingmodules,cardatapacks,alpha,beta):
    """
    This function limits the power of the arms that causes exterme unbalances
    By this control maximum vertical unbalance between two arms of the same phase is limited by beta
                    maximum horizontal unbalance between two phases is limited by alpha
    """
    
    step_second=optStepSize.seconds
    #Identify the maximum powers
    p_phs_max ={}
    p_arm_max ={}
    p_cell_max={}
    for j in [1,2,3]:
        p_phs_max[j]=0
        for k in [1,2]:
            p_arm_max[j,k] =0
            p_cell_max[j,k]={}                
            #Loop through the cells
            for n in armcells:
                car=chargingmodules[j,k,n].connected_car[ts]
                
                if car==None:
                    p_cell_max[j,k][n]=0
                else:
                    car_data_pack=cardatapacks[car]
                    car_ini_soc=car.soc[ts]
                    car_bat_cap=car.bCapacity
                    car_des_soc=1.0
                    p_ch_max =car_data_pack['P_Max']       
                    dep_soc=car_des_soc-car_ini_soc
                    seconds_to_full_charge=dep_soc*car_bat_cap/p_ch_max       
                    if seconds_to_full_charge>step_second:
                        p_cell_max[j,k][n]=p_ch_max
                    else:
                        p_cell_max[j,k][n]=p_ch_max*seconds_to_full_charge/step_second
                        
                #Add the cell power to the arm power
                p_arm_max[j,k]+=p_cell_max[j,k][n]
            #Add the arm power to the phase power
            p_phs_max[j]+=p_arm_max[j,k]

    
    #Vertical unbalance check
    p_phs_ver_balanced ={}
    p_arm_ver_balanced ={}
    p_cell_ver_balanced={}
    for j in [1,2,3]:
        up_min_low=p_arm_max[j,1]-p_arm_max[j,2] #Vertical unbalance in phase j
        if up_min_low>beta:
            #Lower arm to be kept constant
            p_arm_ver_balanced[j,2] =p_arm_max[j,2]
            p_cell_ver_balanced[j,2]=p_cell_max[j,2]
            #Upper arm power to be limited
            p_arm_ver_balanced[j,1] =p_arm_max[j,2]+beta
            required_modulation     =p_arm_ver_balanced[j,1]/p_arm_max[j,1]
            p_cell_ver_balanced[j,1]=dict([(n,p*required_modulation) for n,p in p_cell_max[j,1].items()])
        elif up_min_low<-beta:
            #Upper arm to be kept constant
            p_arm_ver_balanced[j,1] =p_arm_max[j,1]
            p_cell_ver_balanced[j,1]=p_cell_max[j,1]
            #Lower arm power to be limited
            p_arm_ver_balanced[j,2] =p_arm_max[j,1]+beta
            required_modulation     =p_arm_ver_balanced[j,2]/p_arm_max[j,2]
            p_cell_ver_balanced[j,2]=dict([(n,p*required_modulation) for n,p in p_cell_max[j,2].items()])
        else:
            #Upper and lower arm to be kept constant
            p_arm_ver_balanced[j,1] =p_arm_max[j,1]
            p_arm_ver_balanced[j,2] =p_arm_max[j,2]
            p_cell_ver_balanced[j,1]=p_cell_max[j,1]
            p_cell_ver_balanced[j,2]=p_cell_max[j,2]
        p_phs_ver_balanced[j]=p_arm_ver_balanced[j,1]+p_arm_ver_balanced[j,2]
        
        
    #Horizontal unbalance check
    p_phs_old_iteration =p_phs_ver_balanced.copy()
    p_arm_old_iteration =p_arm_ver_balanced.copy()
    p_cell_old_iteration=p_cell_ver_balanced.copy()
    p_phs_new_iteration =p_phs_ver_balanced.copy()
    p_arm_new_iteration =p_arm_ver_balanced.copy()
    p_cell_new_iteration=p_cell_ver_balanced.copy()
    
    #If unbalance is within tolerable limits
    a_min_b=p_phs_old_iteration[1]-p_phs_old_iteration[2]
    a_min_c=p_phs_old_iteration[1]-p_phs_old_iteration[3]
    b_min_c=p_phs_old_iteration[2]-p_phs_old_iteration[3]
    if abs(a_min_b)<=alpha and abs(a_min_c)<=alpha and abs(b_min_c)<=alpha:
        convergence=True
    else:
        convergence=False
        iteration=0
       
    while(convergence==False):
        iteration+=1
        #print("Iteration",iteration)
        
        for j1,j2 in [(1,2),(1,3),(2,3)]:
            
            le_min_ri=p_phs_old_iteration[j1]-p_phs_old_iteration[j2] #Horizontal unbalance between j1 and j2
            if le_min_ri>alpha:
                #Phase j2 to be kept constant
                p_phs_new_iteration [j2]  =p_phs_old_iteration[j2]
                p_arm_new_iteration [j2,1]=p_arm_old_iteration [j2,1]
                p_arm_new_iteration [j2,2]=p_arm_old_iteration [j2,2]
                p_cell_new_iteration[j2,1]=p_cell_old_iteration[j2,1]
                p_cell_new_iteration[j2,2]=p_cell_old_iteration[j2,2]
                #Phase j1 to be limited
                p_phs_new_iteration[j1]   =p_phs_new_iteration[j2]+alpha
                required_modulation       =p_phs_new_iteration[j1]/p_phs_old_iteration[j1]
                p_arm_new_iteration[j1,1] =p_arm_old_iteration[j1,1]*required_modulation
                p_arm_new_iteration[j1,2] =p_arm_old_iteration[j1,2]*required_modulation
                p_cell_new_iteration[j1,1]=dict([(n,p*required_modulation) for n,p in p_cell_old_iteration[j1,1].items()])
                p_cell_new_iteration[j1,2]=dict([(n,p*required_modulation) for n,p in p_cell_old_iteration[j1,2].items()])
            elif le_min_ri<-alpha:
                #Phase j1 to be kept constant
                p_phs_new_iteration [j1]  =p_phs_old_iteration[j1]
                p_arm_new_iteration [j1,1]=p_arm_old_iteration [j1,1]
                p_arm_new_iteration [j1,2]=p_arm_old_iteration [j1,2]
                p_cell_new_iteration[j1,1]=p_cell_old_iteration[j1,1]
                p_cell_new_iteration[j1,2]=p_cell_old_iteration[j1,2]
                #Phase j2 to be limited
                p_phs_new_iteration[j2]   =p_phs_new_iteration[j1]+alpha
                required_modulation       =p_phs_new_iteration[j2]/p_phs_old_iteration[j2]
                p_arm_new_iteration[j2,1] =p_arm_old_iteration[j2,1]*required_modulation
                p_arm_new_iteration[j2,2] =p_arm_old_iteration[j2,2]*required_modulation
                p_cell_new_iteration[j2,1]=dict([(n,p*required_modulation) for n,p in p_cell_old_iteration[j2,1].items()])
                p_cell_new_iteration[j2,2]=dict([(n,p*required_modulation) for n,p in p_cell_old_iteration[j2,2].items()])
            else:
                #Both phases to be kept constant
                for j in [j1,j2]:
                    p_phs_new_iteration[j]=p_phs_old_iteration[j]
                    p_arm_new_iteration[j,1] =p_arm_old_iteration[j,1]
                    p_arm_new_iteration[j,2] =p_arm_old_iteration[j,2]
                    p_cell_new_iteration[j,1]=p_cell_old_iteration[j,1]
                    p_cell_new_iteration[j,2]=p_cell_old_iteration[j,2]
                
            p_phs_old_iteration =p_phs_new_iteration.copy()
            p_arm_old_iteration =p_arm_new_iteration.copy()
            p_cell_old_iteration=p_cell_new_iteration.copy()
            
        a_min_b=p_phs_new_iteration[1]-p_phs_new_iteration[2]
        a_min_c=p_phs_new_iteration[1]-p_phs_new_iteration[3]
        b_min_c=p_phs_new_iteration[2]-p_phs_new_iteration[3]
        if abs(a_min_b)<=alpha*1.001 and abs(a_min_c)<=alpha*1.001 and abs(b_min_c)<=alpha*1.001:
            convergence=True
        else:
            convergence=False
            if iteration>1000:
                raise("More than 1000 loops")
            
                     
    #p_phs_ver_hor_balanced =p_phs_new_iteration
    #p_arm_ver_hor_balanced =p_arm_new_iteration
    p_cell_ver_hor_balanced=p_cell_new_iteration
    
    return p_cell_ver_hor_balanced


def let_unbalance_for_dummy_charging(ts,optStepSize,armcells,chargingmodules,cardatapacks):
    """
    This function calculates maximum feasible input power to the cars
    """

    step_second=optStepSize.seconds
    #Identify the maximum powers
    p_cell_max={}
    for j in [1,2,3]:
        for k in [1,2]:
            p_cell_max[j,k]={}                
            #Loop through the cells
            for n in armcells:
                car=chargingmodules[j,k,n].connected_car[ts]
                
                if car==None:
                    p_cell_max[j,k][n]=0
                else:
                    car_data_pack=cardatapacks[car]
                    car_ini_soc=car.soc[ts]
                    car_bat_cap=car.bCapacity
                    car_des_soc=1.0
                    p_ch_max =car_data_pack['P_Max']       
                    dep_soc=car_des_soc-car_ini_soc
                    seconds_to_full_charge=dep_soc*car_bat_cap/p_ch_max       
                    if seconds_to_full_charge>step_second:
                        p_cell_max[j,k][n]=p_ch_max
                    else:
                        p_cell_max[j,k][n]=p_ch_max*seconds_to_full_charge/step_second                        
    
    return p_cell_max

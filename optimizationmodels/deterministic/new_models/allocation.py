# -*- coding: utf-8 -*-
"""
Created on Sat May 16 09:08:24 2020

@author: egu
"""

from pyomo.environ import SolverFactory
from pyomo.core import *
import pyomo.kernel as pmo
from itertools import product

import numpy as np
import pandas as pd

def choose_an_arm(opt_solver,opt_horizon,optW0,optW1,armsize,carschedule,carleave,armschedules,armnumbers):     
    ###########################################################################
    ####################Constructing the optimization model####################
    model = ConcreteModel()

    model.J =Set(initialize=[1,2,3])     #Index set for the phases i.e. 1,2,3
    model.K =Set(initialize=[1,2])       #Index set for the arms   i.e. 1,2   
    model.T =Set(initialize=opt_horizon,ordered=True) #Index set for the time steps in opzimization horizon    
    
    #Car number unbalance parameters
    model.w_H   =optW0      #Penalization factor for horizonal unbalance
    model.w_V   =optW1      #Penalization factor for vertical unbalance
    model.N     =armsize    #Number of cells in each arm 
       
    model.n_arm_old =armnumbers     #Parameter: Number of connected EVs in each arm (only considering already allocated cars) at initial step
    model.p_arm_old =armschedules   #Parameter: Aggregated arm schedule in each arm (only considering already allocated cars) at each time step
    #model.b         =carpresence    #Parameter: Indexed binaries to define new car's presence in the car at each time step
    model.s         =carschedule    #Parameter: Schedule of the new car
    model.leave     =carleave       #Parameter: Time step at which new car will leave the park

    #Variables
    model.x         =Var(model.J,model.K,within=pmo.Binary) #Binary variable to define the allocation of the car to arm 'k' of phase 'j'
    model.p_arm_new =Var(model.J,model.K,model.T)           #Variables to define the new arm schedules after the allocation
    model.p_phs_new =Var(model.J,model.T)           #Variables to define the new phs schedules after the allocation      
    
    model.unb_hor   =Var(model.J,model.J,model.T)           #Horizontal unbalance: difference in the phase powers at time step 't' after new allocation
    model.unb_ver   =Var(model.J,model.T)                   #Vertical   unbalance: difference in the arm powers of two arms of a phase at time step 't'after new allocation
    
    #Constraints
    def combinatorics(model):       #A car cannot be assigned more than 1 arm
        return sum(model.x[j,k] for j,k in product(model.J,model.K))==1
    model.comb_const=Constraint(rule=combinatorics)
    
    def arm_limit(model,j,k):     #An arm could not host more cars than the number of the submodules in the arm
        return model.n_arm_old[j,k]+model.x[j,k]<=model.N
    model.armlimit=Constraint(model.J,model.K,rule=arm_limit)
    
    def p_arm_ch(model,j,k,t):         #Number of the charging cars in arm 'k' of phase 'j' in all allocation scenarios
        if t<model.leave:
            return model.p_arm_new[j,k,t]==model.x[j,k]*model.s[t]+model.p_arm_old[j,k][t]
        else:
            return model.p_arm_new[j,k,t]==model.p_arm_old[j,k][t]
    model.armtotal_ch=Constraint(model.J,model.K,model.T,rule=p_arm_ch)
         
    def p_phs_ch(model,j,t):          #Phase power schedule of phase j
        return model.p_phs_new[j,t]==model.p_arm_new[j,1,t]+model.p_arm_new[j,2,t]
    model.phasetotal=Constraint(model.J,model.T,rule=p_phs_ch)
   
    def delta_phs_phs(model,j1,j2,t):   #Car number imbalance phase pairs
        return model.unb_hor[j1,j2,t]==model.p_phs_new[j1,t]-model.p_phs_new[j2,t]
    model.horizontal_balance=Constraint(model.J,model.J,model.T,rule=delta_phs_phs)

    def delta_arm_arm(model,j,t):   #Car number imbalance between arms of each phases
        return model.unb_ver[j,t]==model.p_arm_new[j,1,t]-model.p_arm_new[j,2,t]
    model.vertical_balance=Constraint(model.J,model.T,rule=delta_arm_arm)
          
    #OBJECTIVE FUNCTION
    def obj_rule(model):
        #Expression for inter-phase imbalance --> DC component of balancing current
        expr0= sum(model.w_H *(model.unb_hor[1,2,t]*model.unb_hor[1,2,t]+
                               model.unb_hor[1,3,t]*model.unb_hor[1,3,t]+
                               model.unb_hor[2,3,t]*model.unb_hor[2,3,t]) for t in model.T)
        
        #Expression for in-phase imbalance    --> Fundamental freq component of balancing current
        expr1= sum(model.w_V *(model.unb_ver[1,t]*model.unb_ver[1,t]+
                               model.unb_ver[2,t]*model.unb_ver[2,t]+
                               model.unb_ver[3,t]*model.unb_ver[3,t]) for t in model.T)
        return expr0+expr1
    model.obj=Objective(rule=obj_rule, sense = minimize)
    
    ###########################################################################
    ######################Solving the optimization model ######################            
    opt_solver.solve(model)

    ###########################################################################
    
    ###########################################################################
    ################################Returning the results#########################      
    #new_car_loading     =np.array(car_if_ch)
    #park_new_loading    =park_old_loading
    for j,k in product(model.J,model.K):
        if round(model.x[j,k]())==1:
            allocation_to=j,k
            #park_new_loading[j,k]=park_new_loading[j,k]+new_car_loading
    return allocation_to
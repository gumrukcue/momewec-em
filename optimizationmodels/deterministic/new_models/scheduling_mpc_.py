# -*- coding: utf-8 -*-
"""
Created on Wed May 20 09:30:08 2020

@author: egu
"""

from pyomo.environ import SolverFactory
from pyomo.core import *
import numpy as np
import pandas as pd
from itertools import product

def mpc_optimize_schedules(parkdata,connections,solver):

    
    ###########################################################################
    ################################Data formatting############################
    phases    =[1,2,3]
    arms      =[1,2]
    cells    =parkdata['cells']       #Cells in an arm
    horizon  =parkdata['opt_horizon']
    deltaT   =parkdata['opt_step']        
    alpha_p  =parkdata['alpha']
    beta_p   =parkdata['beta']
    ch_eff   =parkdata['eff_ch']
    arm_cap  =parkdata['arm_cap']
            
    #Car data
    carpositions={}  
    pmax  ={}
    bcap  ={}
    dep   ={}
    dessoc={}
    inisoc={}
    refsoc={}
    for a in connections.keys():
        pha,arm,ce  =connections[a]['Cell']
        pmax[a]     =connections[a]['P_Max']
        bcap[a]     =connections[a]['BCap'] 
        dep[a]      =connections[a]['leavets']
        dessoc[a]   =connections[a]['finSoC']
        inisoc[a]   =connections[a]['SoC']
        refsoc[a]   =connections[a]['ReferenceSOC']
        for j in phases:
            for k in arms:
                if  pha==j and arm==k:
                    carpositions[a,j,k]=1
                else:
                    carpositions[a,j,k,]=0
        
           
    #price_kWs=dict(enumerate(parkdata['price']/1000/3600))
    ###########################################################################    
     
    ###########################################################################
    ####################Constructing the optimization model####################
    model = ConcreteModel()

    model.J =Set(initialize=phases)  #Index set for the phases e.g. 'R','S','T'
    model.K =Set(initialize=arms)    #Index set for the arms   e.g. 'u','l'
    model.N =Set(initialize=cells)   #Index set for the slots  e.g. 1,2,3,...N
    model.A =Set(initialize=sorted(connections.keys()))

    #Time parameters
    model.deltaSec=deltaT.seconds                            #Time discretization (Size of one time step in seconds)   
    model.T       =Set(initialize=horizon[:-1],ordered=True) #Index set for the time steps in opzimization horizon
    model.Tp      =Set(initialize=horizon,ordered=True)      #Index set for the time steps in opzimization horizon for SoC

    #Power import and imbalance parameters
    model.alpha_p  =alpha_p        #Maximum power imbalance between two phases
    model.beta_p   =beta_p         #Maximum power imbalance between two arms of a phase
    model.ch_eff   =ch_eff         #Charging module efficiency
    model.arm_cap  =arm_cap        #Maximum arm power
    #model.elprice  =price_kWs      #Energy price EUR/kWs
        
    #Car parameters
    model.P_Car_Max=pmax     #Maximum charging power to EV battery
    model.W        =bcap     #Energy capacity of EV batteries
    model.departure=dep      #Departure times of EVs
    model.desSoC   =dessoc   #Desired SOC of EVs
        
    #Real time parameters        
    model.iniSoC   =inisoc   #SoC when the optimization starts
    
    #Reference parameters
    model.desSoC   =dessoc   #Desired SOC of EVs
    model.refSoC   =refsoc   #Refernce SOC trajectory
        
    #Variables
    model.s    =Var(model.A,model.T,within=NonNegativeReals)     #Charging power to the car a
    model.p_arm=Var(model.J,model.K,model.T,within=NonNegativeReals)             #Power flows into the arm k of phase j
    model.P_MMC=Var(model.T,within=NonNegativeReals)                             #Total system power  
    model.SoC  =Var(model.A,model.Tp,bounds=(0,1)) 
    model.dev  =Var(model.A,model.Tp,within=Reals)
    #model.fdev =Var(model.A,within=Reals)
                           
    #CONSTRAINTS
    def storageConservation(model,a,t):    #SOC of EV batteries will change with respect to the charged power and battery energy capacity
        if t==min(horizon):
            return model.SoC[a,t]==model.iniSoC[a]
        else:
            return model.SoC[a,t]==(model.SoC[a,t - 1] + model.s[a,t-1]/model.W[a] *model.deltaSec)
    model.socconst=Constraint(model.A,model.Tp,rule=storageConservation)
    
    def chargepowerlimit(model,a,t):   #Cars can only be charged until the departure
        if t<model.departure[a]:
            return model.s[a,t]<=model.P_Car_Max[a]
        else:
            return model.s[a,t]==0
    model.carpowconst=Constraint(model.A,model.T,rule=chargepowerlimit)
            
    def armpower(model,j,k,t):          #Constraint for arm power: Summation of the powers charging cells of an arm
        return model.p_arm[j,k,t]==sum(carpositions[a,j,k]*model.s[a,t]/model.ch_eff for a in model.A)
    model.armpowtotal=Constraint(model.J,model.K,model.T,rule=armpower)
    
    def armpower_undersizing(model,j,k,t):  #Constraint for arm power: Summation of the powers charging cells of an arm
        return model.p_arm[j,k,t]<=model.arm_cap
    model.armpowcap =Constraint(model.J,model.K,model.T,rule=armpower_undersizing)
    
    def totpower(model,t):              #Constraint for total power to MMC     #DONE
        return model.P_MMC[t]==sum(model.p_arm[j,k,t] for j,k in product(model.J,model.K))
    model.mmcpowtotal=Constraint(model.T,rule=totpower)
    
    def phasepowersymmetry12(model,t):    #Constraint for power imbalance between phase pairs 
        return -model.alpha_p<=model.p_arm[1,1,t]+model.p_arm[1,2,t]-model.p_arm[2,1,t]-model.p_arm[2,2,t]<=model.alpha_p
    def phasepowersymmetry13(model,t):    #Constraint for power imbalance between phase pairs 
        return -model.alpha_p<=model.p_arm[1,1,t]+model.p_arm[1,2,t]-model.p_arm[3,1,t]-model.p_arm[3,2,t]<=model.alpha_p
    def phasepowersymmetry23(model,t):    #Constraint for power imbalance between phase pairs 
        return -model.alpha_p<=model.p_arm[2,1,t]+model.p_arm[2,2,t]-model.p_arm[3,1,t]-model.p_arm[3,2,t]
    model.phasePcon_12=Constraint(model.T,rule=phasepowersymmetry12)
    model.phasePcon_13=Constraint(model.T,rule=phasepowersymmetry13)
    model.phasePcon_23=Constraint(model.T,rule=phasepowersymmetry23)
    
    def armpowersymmetry(model,j,t):    #Constraint for power imbalance between two arms of the same phase
        return -model.beta_p<=model.p_arm[j,1,t]-model.p_arm[j,2,t]<=model.beta_p
    model.armPcon=Constraint(model.J,model.T,rule=armpowersymmetry)
    
    def reftrack(model,a,t):
        if t<model.departure[a]: 
            return model.dev[a,t]==model.refSoC[a][t]-model.SoC[a,t]
        else:
            return model.dev[a,t]==model.desSoC[a]-model.SoC[a,t]
    model.refcon =Constraint(model.A,model.Tp,rule=reftrack)
    
    def dessocref(model,a):
        return model.fdev[a]==model.desSoC[a]-model.SoC[a,max(horizon)]
    #model.finsocc=Constraint(model.A,rule=dessocref)     
     
    #OBJECTIVE FUNCTION
    def obj_rule(model):  
        #return sum(model.dev[a,t]*model.dev[a,t] for a,t in product(model.A,model.T))
        return sum(model.dev[a,max(horizon)]*model.dev[a,max(horizon)] for a in model.A)
    model.obj=Objective(rule=obj_rule, sense = minimize)
    
    ###########################################################################         
     
    ###########################################################################
    ######################Solving the optimization model ######################            
    #start=time.time()
    solver.solve(model)
    #end=time.time()
    #print("Solution in",end-start)
    ###########################################################################
    
    ###########################################################################
    ################################Saving the results#########################      
    powers   =dict.fromkeys(connections.keys())
    socs     =dict.fromkeys(connections.keys())  
    for a in model.A:
        socs[a]={}
        powers[a]=model.s[a,min(horizon)]()
        for t in model.Tp:
            socs[a][t] =model.SoC[a,t]()
        socs[a]=pd.Series(socs[a])
    return powers,socs
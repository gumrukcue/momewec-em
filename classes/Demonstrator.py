# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 11:36:49 2020

@author: egu
"""

import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.animation import FuncAnimation
from datetime import date, time, timedelta
import numpy as np

class Demonstrator():
    
    def __init__(self,N,cellPower,nodeSize=100):
        self.N=N
        self.P=cellPower
        self.nodeSize=nodeSize
        self.armkeys=[(p,a) for p in [1,2,3] for a in [1,2]]
        
        
        self.arm_powers={}
        self.arm_numbers={}
        
         
        self.pos={}          #Positions of the nodes (each standing for a cell) in graphs
        self.Graphs={}
        for p,a in sorted(self.armkeys):
            if a==1:
                self.pos[p, a] = dict([n, (p,    n+1)] for n in range(self.N))
            else:
                self.pos[p, a] = dict([n, (p, -(n+1))] for n in range(self.N))
            
            #One graph for each arm
            self.Graphs[p,a] = nx.Graph()
            self.Graphs[p,a].add_nodes_from(sorted(self.pos[p,a].keys()))
            for n in self.pos[p,a].keys():
                self.Graphs[p,a].node[n]['pos']=self.pos[p,a][n]
                        
        #####   Figures  #####
        self.fig = plt.figure(figsize=(18,10))
        
        self.axs0 =self.fig.add_subplot(1,3,1)
#        self.axs0.set_title("Event screen")
#        self.axs0.set_yticks([])
#        self.axs0.set_xticks([])
        self.axs0.set_axis_off()
        self.time_text   =self.axs0.text(-0.02, 0.95, '', transform=self.axs0.transAxes,weight='bold')
        self.arrt_text   =self.axs0.text(-0.02, 0.90, 'Arrivals', transform=self.axs0.transAxes,weight='bold')        
        self.arrivaltable=self.axs0.table(cellText=np.array([['']*4]*4),
                                          colLabels=['ID','IniSoC','DesSoC','Est. Departure'],
                                          colWidths=[0.30,0.2,0.2,0.56],
                                          bbox=[-0.02,0.49,0.96,0.4])
        self.arrivaltable.auto_set_font_size(False)
        self.arrivaltable.set_fontsize(10)
        #self.arr_table   =self.axs0.text(0.02, 0.90, 'ID', transform=self.axs0.transAxes,weight='bold')
            
        self.axs1 =self.fig.add_subplot(1,3,2)
        self.axs1.set_title("Connected cells")
        self.axs1.set_xlabel("Phases")
        self.axs1.set_ybound(-self.N-1,self.N+1)
        self.axs1.set_yticks(list(range(-self.N,self.N+1)))
        self.axs1.set_xticks([1,2,3])
        self.axs1.set_xbound(0,4)
        self.axs1.grid(linestyle='--')
        
        self.axs2=self.fig.add_subplot(1,3,3)
        self.axs2.set_title("Arm Power [kW]")
        self.axs2.set_ybound(-self.N*self.P,self.N*self.P)
        self.axs2.set_yticks(list(range(-self.N*self.P,(self.N+1)*self.P,self.P)))
        self.axs2.yaxis.set_label_position("right")
        self.axs2.yaxis.tick_right()
        self.axs2.set_xlabel("Phases")
        self.axs2.set_xticks([1,2,3])
        self.axs2.set_xbound(0,4)
        self.axs2.grid(axis='y',linestyle='--')
        self.bar_u = self.axs2.bar([1, 2, 3], [0, 0, 0], 0.25, label='Upper arm')
        self.bar_l = self.axs2.bar([1, 2, 3], [-0, -0, -0], 0.25, label='Lower arms')
        
        
        for p, a in sorted(self.armkeys):
            nx.draw_networkx_nodes(self.Graphs[p, a], self.pos[p, a], node_size=self.nodeSize, node_shape='o', node_color='y',ax=self.axs1,label='Free Cells')
        
    def animate_arrival(self,timestamp,enteringcars): 

        time_stamp ="Time:"+str(timestamp)
        self.time_text.set_text(time_stamp)
        #nb_of_arrivals="Number of arriving cars:"+str(len(enteringcars))
        nb=1
        for ev in enteringcars:                      
            self.arrivaltable.get_celld()[(nb,0)].get_text().set_text(ev.id)
            self.arrivaltable.get_celld()[(nb,1)].get_text().set_text(ev.soc[timestamp])
            self.arrivaltable.get_celld()[(nb,2)].get_text().set_text(enteringcars[ev]['Desired SoC'])
            self.arrivaltable.get_celld()[(nb,3)].get_text().set_text(enteringcars[ev]['Estimated departure'])
            nb+=1
        for nb in range(nb,5):
            self.arrivaltable.get_celld()[(nb,0)].get_text().set_text('')
            self.arrivaltable.get_celld()[(nb,1)].get_text().set_text('')
            self.arrivaltable.get_celld()[(nb,2)].get_text().set_text('')
            self.arrivaltable.get_celld()[(nb,3)].get_text().set_text('')
              
        self.axs0.plot()
        #plt.pause(0.01)
    
    def animate(self,timestamp,enteringcars,new_con,dif_con,new_load):        
        
        self.arm_powers[timestamp] ={}
        self.arm_numbers[timestamp]={}
        
        for p, a in sorted(self.armkeys):
            self.arm_powers[timestamp][p,a] =new_load[p,a]
            self.arm_numbers[timestamp][p,a]=new_con[p,a]
            
            nx.draw_networkx_nodes(self.Graphs[p, a], self.pos[p, a], nodelist=new_con[p, a],node_size=self.nodeSize/8*3,node_shape='o',node_color='b',ax=self.axs1,label='Connected Cells')
            nx.draw_networkx_nodes(self.Graphs[p, a], self.pos[p, a], nodelist=dif_con[p, a],node_size=self.nodeSize/8*3,node_shape='o',node_color='r',ax=self.axs1,label='New Connections')
            
        upperarm_numbers = [new_load[p,1] for p in [1,2,3]]
        lowerarm_numbers = [new_load[p,2] for p in [1,2,3]]
        for i,b in enumerate(self.bar_u):
            b.set_height(upperarm_numbers[i])
        for i,b in enumerate(self.bar_l):
            b.set_height(-lowerarm_numbers[i])

        
        self.axs1.plot()
        self.axs2.plot()
        plt.pause(0.01)
        
        


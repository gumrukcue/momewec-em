# -*- coding: utf-8 -*-
"""
Created on Wed Dec 4 10:02:19 2019

@author: egu-cse
"""
import random

class Car(object):
    
    def __init__(self,timer,carID,model,max_ac_charge,max_dc_charge,bCapacity,unitconsumption,max_discharge,minSoC,maxSoC,discount,is_shared_vehicle):
        """
        This method initializes the car objects.
        """
        self.timer = timer
        self.id = carID
        self.model = model
        self.max_ac_charge = max_ac_charge
        self.max_dc_charge = max_dc_charge
        self.bCapacity = bCapacity*3600     #kWh to kWs
        self.unitconsumption = unitconsumption
        self.max_discharge = max_discharge
        self.minSoC = minSoC
        self.maxSoC = maxSoC
        self.discount = discount
        self.is_shared_vehicle = is_shared_vehicle
        
        self.soc={self.timer.now:random.randint(20,80)/100}
        
    def assign_temp_data_pack(self,dataid):#,est_dep):
        self.dataid=dataid
        #self.estimated_dep=est_dep
    
    def retrieve_soc_data(self,ts_dst):
        self.soc[ts_dst]=self.soc[ts_dst-self.timer.dT]
    
    def charge(self,k,p_in):
        self.soc[k+self.timer.dT]=self.soc[k]+p_in*self.timer.deltaSec/self.bCapacity 
    
    def idle(self,k):      
        self.soc[k+self.timer.dT]=self.soc[k]
        
"""
Renault ZOE ZE50 R110
"""
class RenaultZOE(Car):
    model = "RenaultZOE"
    max_ac_charge = 22
    max_dc_charge = 50
    max_discharge = 0.0
    bCapacity = 55.0
    minSoC = 0.0
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 16.3 kWh/100km
    # Highway  - Cold Weather 23.1 kWh/100km
    # City     - Mild Weather 10.6 kWh/100km
    # Highway  - Mild Weather 17.9 kWh/100km
    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 16.3 / 100,
        "Highway-Cold": 23.1 / 100,
        "City-Mild": 10.6 / 100,
        "Highway-Mild": 17.9 / 100,
    }
    

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            RenaultZOE.model,
            RenaultZOE.max_ac_charge,
            RenaultZOE.max_dc_charge,
            RenaultZOE.bCapacity,
            RenaultZOE.unitconsumption,
            RenaultZOE.max_discharge,
            RenaultZOE.minSoC,
            RenaultZOE.maxSoC,
            discount,
            is_shared_vehicle,
        )


"""
Hyundai IONIQ Electric
"""
class HyundaiIONIQ(Car):
    model = "HyundaiIONIQ"
    max_ac_charge = 7.2
    max_dc_charge = 44
    max_discharge = 0.0
    bCapacity = 38.3
    minSoC = 0.0
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 15.3 kWh/100km
    # Highway  - Cold Weather 20.7 kWh/100km
    # City     - Mild Weather 9.8 kWh/100km
    # Highway  - Mild Weather 16 Wh/mi

    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 15.3 / 100,
        "Highway-Cold": 20.7 / 100,
        "City-Mild": 9.8 / 100,
        "Highway-Mild": 16 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            HyundaiIONIQ.model,
            HyundaiIONIQ.max_ac_charge,
            HyundaiIONIQ.max_dc_charge,
            HyundaiIONIQ.bCapacity,
            HyundaiIONIQ.unitconsumption,
            HyundaiIONIQ.max_discharge,
            HyundaiIONIQ.minSoC,
            HyundaiIONIQ.maxSoC,
            discount,
            is_shared_vehicle,
        )

"""
Nissan Leaf
"""
class NissanLeaf(Car):
    model = "NissanLeaf"
    max_ac_charge = 12
    max_dc_charge = 46
    max_discharge = 0.0
    bCapacity = 24
    minSoC = 0.0
    maxSoC = 1.0

    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 16.7 / 100,
        "Highway-Cold": 23.2 / 100,
        "City-Mild": 11.1 / 100,
        "Highway-Mild": 18 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            NissanLeaf.model,
            NissanLeaf.max_ac_charge,
            NissanLeaf.max_dc_charge,
            NissanLeaf.bCapacity,
            NissanLeaf.unitconsumption,
            NissanLeaf.max_discharge,
            NissanLeaf.minSoC,
            NissanLeaf.maxSoC,
            discount,
            is_shared_vehicle,
        )



"""
Mercedes EQC 400 4MATIC
"""
class MercedesEQC400(Car):
    model = "MercedesEQC400"
    max_ac_charge = 7.4
    max_dc_charge = 112
    max_discharge = 0.0
    bCapacity = 80.0
    minSoC = 0.0
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 22.2 kWh/100km
    # Highway  - Cold Weather 30.8 kWh/100km
    # City     - Mild Weather 15.5 kWh/100km
    # Highway  - Mild Weather 24.6 kWh/100km
    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 22.2 / 100,
        "Highway-Cold": 30.8 / 100,
        "City-Mild": 15.5 / 100,
        "Highway-Mild": 24.6 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            MercedesEQC400.model,
            MercedesEQC400.max_ac_charge,
            MercedesEQC400.max_dc_charge,
            MercedesEQC400.bCapacity,
            MercedesEQC400.unitconsumption,
            MercedesEQC400.max_discharge,
            MercedesEQC400.minSoC,
            MercedesEQC400.maxSoC,
            discount,
            is_shared_vehicle,
        )


"""
Peugeot e-208
"""
class PeugeotE208(Car):
    model = "PeugeotE208"
    max_ac_charge = 7.4
    max_dc_charge = 100
    max_discharge = 0.0
    bCapacity = 47.5
    minSoC = 0.0
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 16.4 kWh/100km
    # Highway  - Cold Weather 22.6 kWh/100km
    # City     - Mild Weather 10.6 kWh/100km
    # Highway  - Mild Weather 17.6 kWh/100km
    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 16.4 / 100,
        "Highway-Cold": 22.6 / 100,
        "City-Mild": 10.6 / 100,
        "Highway-Mild": 17.6 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            PeugeotE208.model,
            PeugeotE208.max_ac_charge,
            PeugeotE208.max_dc_charge,
            PeugeotE208.bCapacity,
            PeugeotE208.unitconsumption,
            PeugeotE208.max_discharge,
            PeugeotE208.minSoC,
            PeugeotE208.maxSoC,
            discount,
            is_shared_vehicle,
        )
        
        

class VolkswageneUp(Car):
    model = "VolkswageneUp"
    max_ac_charge = 7.2
    max_dc_charge = 40
    max_discharge = 0.0
    bCapacity = 32.3
    minSoC = 0.2
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 16.2 kWh/100km
    # Highway  - Cold Weather 23.1 kWh/100km
    # City     - Mild Weather 10.4 kWh/100km
    # Highway  - Mild Weather 17.9 kWh/100km
    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 16.2 / 100,
        "Highway-Cold": 23.1 / 100,
        "City-Mild": 10.4 / 100,
        "Highway-Mild": 17.9 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            VolkswageneUp.model,
            VolkswageneUp.max_ac_charge,
            VolkswageneUp.max_dc_charge,
            VolkswageneUp.bCapacity,
            VolkswageneUp.unitconsumption,
            VolkswageneUp.max_discharge,
            VolkswageneUp.minSoC,
            VolkswageneUp.maxSoC,
            discount,
            is_shared_vehicle,
        )

"""
Tesla Model 3 Standard Range Plus
"""


class Tesla3(Car):
    model = "Tesla3"
    max_ac_charge = 12
    max_dc_charge = 170
    max_discharge = 0.0
    bCapacity = 47.5
    minSoC = 0.0
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 15.8 kWh/100km
    # Highway  - Cold Weather 21.1 kWh/100km
    # City     - Mild Weather 10.2 kWh/100km
    # Highway  - Mild Weather 16.1 kWh/100km
    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 15.8 / 100,
        "Highway-Cold": 21.1 / 100,
        "City-Mild": 10.2 / 100,
        "Highway-Mild": 16.1 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            Tesla3.model,
            Tesla3.max_ac_charge,
            Tesla3.max_dc_charge,
            Tesla3.bCapacity,
            Tesla3.unitconsumption,
            Tesla3.max_discharge,
            Tesla3.minSoC,
            Tesla3.maxSoC,
            discount,
            is_shared_vehicle,
        )
        
        
"""
BMW i3 120 Ah
"""


class BMWi3(Car):
    model = "BMWi3"
    max_ac_charge = 12
    max_dc_charge = 49
    max_discharge = 0.0
    bCapacity = 37.9
    minSoC = 0.0
    maxSoC = 1.0
    # Energy consumption data
    # City     - Cold Weather 16.1 kWh/100km
    # Highway  - Cold Weather 23 kWh/100km
    # City     - Mild Weather 10.4 kWh/100km
    # Highway  - Mild Weather 17.6 kWh/100km
    # Values are kWh/km
    unitconsumption = {
        "City-Cold": 16.1 / 100,
        "Highway-Cold": 23 / 100,
        "City-Mild": 10.4 / 100,
        "Highway-Mild": 17.6 / 100,
    }

    def __init__(self, timer, VehicleID, discount=0.0, is_shared_vehicle=False):
        super().__init__(
            timer,
            VehicleID,
            BMWi3.model,
            BMWi3.max_ac_charge,
            BMWi3.max_dc_charge,
            BMWi3.bCapacity,
            BMWi3.unitconsumption,
            BMWi3.max_discharge,
            BMWi3.minSoC,
            BMWi3.maxSoC,
            discount,
            is_shared_vehicle,
        )

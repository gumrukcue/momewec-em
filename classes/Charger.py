# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 15:27:22 2019

@author: egu
"""

class ChargingModule(object):
    
    def __init__(self,timer,P_max,chargingEfficiency=1.0):
        
        self.timer=timer
        self.P_max=P_max
        self.eff_ch=chargingEfficiency
        
        self.connected_car ={self.timer.now:None}
        self.supplied_power={self.timer.now:None}
        
    def retrieve_connection_data(self,ts_dst):
        self.connected_car[ts_dst]=self.connected_car[ts_dst-self.timer.dT]
        self.supplied_power[ts_dst]=None
        
    def connect(self,ts,car):      
        self.connected_car[ts]=car
    
    def disconnect(self,ts):
        self.connected_car[ts]=None
    
    def supply(self,ts,P_out):
        self.supplied_power[ts]=P_out
        self.connected_car[ts].charge(ts,P_out*self.eff_ch)
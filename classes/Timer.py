# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 16:05:05 2019

@author: egu
"""
from datetime import datetime, timedelta

class Timer(object):
    
    def __init__(self,year,month,day,hour=0,minute=0,timediscretization=60):
        """
        Timer class specifies the discretization over time, number of time steps to be shifted
        :param timediscretization: 
            integer: number of seconds in one time step --> default 1 minutes
        :param reschedulinghorizon: 
            integer: number of time steps of total rescheduling horizon --> default 24 hours
        """
        self.dT=timedelta(seconds=timediscretization)
        self.deltaSec=timediscretization   
        self.now=datetime(year,month,day,hour,minute)
        
    def updateTimer(self):
        """
        Updates the shifted number of time steps i.e. start time step of the new rescheduling horizon
        """
        self.now+=self.dT
        #print("Timer updated",self.now)
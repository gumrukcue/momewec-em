# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 09:02:03 2019

@author: egu
"""

import socket
import pickle
from datetime import date, datetime, timedelta
import numpy as np
import pandas as pd

class SchedulingClient(object):
    def __init__(self,armsize,cellpow,celleff,mmcpower):
        """
        :param armsize:     number of the submodules in one arm
        :param cellpower:   nominal charging power of MMC submodules in kW
        :param celleff:     charging efficiency of MMC submodules
        :param mmcpower:    maximum power input to whole MMC
        """
        self.armsize  =armsize
        self.cellpower=cellpow
        self.celleff  =celleff
        self.mmcpower =mmcpower
             
        self.client  =socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect_to_server(self,host,port):
        print(datetime.now())
        self.client.connect((host,port))
        print("Connected to server at", host, port)

    def save_charging_schedule(self,new_schedule):
        """
        :param new_schedule: Dictionary {'carID': np.array[] for future time steps)
        """
        self.charging_schedule = new_schedule

    def get_charging_schedules(self):
        """
        Returns the charging schedules for all allocated cars
        """
        df=pd.DataFrame()
        for car_id in sorted(self.charging_schedule.keys()):
            df[car_id]=pd.Series(self.charging_schedule[car_id])
        return df

    def pack_data(self,optTimeSec,optHorizon,opt0tol,opt1tol,connectiondict,reqTimeStamp):
        """
        Solver to be specified in the server side
        :param optTimeSec:      optimization time step size in seconds
        :param optHorizon:      optimization horizon in number of time steps
        :param opt0tol:         maximum acceptable horizontal imbalance
        :param opt1tol:         maximum acceptable vertical imbalance
        :param connectiondict:  dictionary for allocated cars' data
        :param reqTimeStamp:    Time stamp for the request time (datetime object)
        """
        
        sendDict=connectiondict.copy()
        sendDict['Slots']=list(range(1,self.armsize+1))#
        sendDict['TimeInterval']=optTimeSec#
        sendDict['OptimizationHorizon']=optHorizon#
        sendDict['P_MMC_Max']=self.mmcpower#
        sendDict['alpha_p']=opt0tol#
        sendDict['beta_p'] =opt1tol#
        sendDict['Charging Efficiency']=self.celleff#
        sendDict['ReqID']  =str(reqTimeStamp)
              
        self.temp_message=pickle.dumps(sendDict)
        #print(self.temp_message)

    def send_optimization_request_for_scheduling(self):
        print(datetime.now(),": sending request")
        self.optimized=False
        self.client.sendall(self.temp_message)

    def receive_optimization_results_for_scheduling(self):
        while (self.optimized == False):
            opt_result_byte = self.client.recv(1024*1024)
            print(datetime.now(),": result received")
            opt_result_dict = pickle.loads(opt_result_byte)
            self.optimized=True
        return pd.DataFrame(opt_result_dict)
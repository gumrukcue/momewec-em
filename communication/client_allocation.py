# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 08:23:38 2019

@author: egu
"""

import socket
import pickle
from datetime import date, datetime, timedelta
import numpy as np
import pandas as pd

class AllocationClient(object):
    def __init__(self,armsize,cellpower):
        """
        :param armsize:     number of the submodules in one arm
        :param cellpower:   nominal charging power of MMC submodules in kW
        """
        self.armsize =armsize
        self.cellPNom=cellpower
        self.actual_load_before_rescheduling=dict.fromkeys([(p,a) for p in [1,2,3] for a in [1,2]])

        self.client  =socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect_to_server(self,host,port):
        print(datetime.now())
        self.client.connect((host,port))
        print("Connected to server at", host, port)

    def pack_data(self,optTimeSec,optHorizon,optW0,optW1,carBCap,carISoC,carFSoC,carDep,tempCPlan,actAllc,reqTimeStamp):
        """
        Solver to be specified in the server side
        :param optTimeSec:  optimization time step size in seconds
        :param optHorizon:  optimization horizon in number of time steps
        :param optW0:       weight factor for balancing effort to horizontal imbalance
        :param optW1:       weight factor for balancing effort to vertical imbalance
        :param carBCap:     EV battery capacity
        :param carISoC:     EV initial state of charge
        :param carFSoC:     EV desired state of charge
        :param carDep:      EV departure time in number of time steps ahead
        :param tempCPlan:   Dictionary (keys: (p,a) pairs and values: np.array loading for future time steps
        :param actAllc:     Dictionary (keys: (p,a) pairs and values: number of connected cars in the arm
        :param reqTimeStamp:Time stamp for the request time (datetime object)
        """       
        sendDict={}
        sendDict['armsize']     =self.armsize
        sendDict['cellPNom']    =self.cellPNom
        
        sendDict['optTimeSec']  =optTimeSec
        sendDict['optHorizon']  =optHorizon
        sendDict['optW0']       =optW0
        sendDict['optW1']       =optW1
        
        sendDict['carBCap']  =carBCap     
        sendDict['carISoC']  =carISoC
        sendDict['carFSoC']  =carFSoC   
        sendDict['carDep']   =carDep
        
        sendDict['Loading11']=tempCPlan[1,1]
        sendDict['Loading12']=tempCPlan[1,2]
        sendDict['Loading21']=tempCPlan[2,1]
        sendDict['Loading22']=tempCPlan[2,2]
        sendDict['Loading31']=tempCPlan[3,1]
        sendDict['Loading32']=tempCPlan[3,2]
        
        sendDict['Connection']=actAllc
                
        sendDict['ReqID']  =str(reqTimeStamp)
              
        self.temp_message=pickle.dumps(sendDict)

    def send_optimization_request_for_allocation(self):
        print(datetime.now(),": sending request")
        self.optimized=False
        self.client.sendall(self.temp_message)

    def receive_optimization_results_for_allocation(self):
        while (self.optimized == False):
            data = self.client.recv(64)
            decod= data.decode('utf8').split('/')
            phs  =int(decod[0])
            arm  =int(decod[1])
            self.optimized = True

            print(datetime.now(), "optimization result: allocation to phase %s arm %s" % (phs, arm))
            self.client.close()
            return phs,arm
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 07:55:38 2019

@author: egu
"""

import socket
import pickle
import numpy as np
from datetime import date, datetime, timedelta
from pyomo.environ import SolverFactory
from optimizationmodels.deterministic.new_models.allocating_car_fixed import choose_an_arm

class AllocationServer(object):
    
    def __init__(self,host,port,timeoutThr=100):
        self.adress=(host,port)
        self.socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(timeoutThr)
        self.socket.bind(self.adress)
        self.client=None
        self.client_adress=None
                      
    def listen(self,nbofClients):
        self.socket.listen(nbofClients)
        print(datetime.now(),": listening to the connections requests")
        print()
        
    def accept(self):
        connection,adress=self.socket.accept()
        print(datetime.now(),": client at",adress,"is connected")
        self.client=connection
        self.client_adress=adress

    def receive_request(self,nbchar=1024*1024):
        try:
            rec_byte=self.client.recv(nbchar)
            if len(rec_byte) == 0:
                return 0
            print(datetime.now(),": the client at adress %s:%s requested optimization for allocation"%self.client_adress)
            self.actual_request=pickle.loads(rec_byte)
        except:
            print(datetime.now(),": no optimization request ")
           
    def optimize_allocation(self,optSolver):   
        client_dict=self.actual_request
            
        armsize     =client_dict['armsize']
        cellPNom    =client_dict['cellPNom']
        
        optTimeSec  =client_dict['optTimeSec'] 
        optHorizon  =client_dict['optHorizon'] 
        optW0       =client_dict['optW0'] 
        optW1       =client_dict['optW1']
        
        carBCap     =client_dict['carBCap']
        carISoC     =client_dict['carISoC']
        carFSoC     =client_dict['carFSoC']
        carDep      =client_dict['carDep']
        
        iniLoading={}
        iniLoading[1,1]=client_dict['Loading11']
        iniLoading[1,2]=client_dict['Loading12']
        iniLoading[2,1]=client_dict['Loading21']
        iniLoading[2,2]=client_dict['Loading22']
        iniLoading[3,1]=client_dict['Loading31']
        iniLoading[3,2]=client_dict['Loading32']
        
        actAllocation=client_dict['Connection']
               
        self.actual_request_id=client_dict['ReqID']
        
        #print(datetime.now(),": optimization function being called for the instance",client_dict['ReqID'])       
        allocation_to=choose_an_arm(optSolver,optTimeSec,optHorizon,optW0,optW1,armsize,cellPNom,carBCap,carISoC,carFSoC,carDep,iniLoading,actAllocation)        
        #print(datetime.now(),": optimization function performed")
        
        phs=allocation_to[0]
        arm=allocation_to[1]
        fullmessage=str(phs)+'/'+str(arm)
               
        self.client.send(fullmessage.encode('utf8'))
        print(datetime.now(),": optimization result has been sent to the client")
               
        
        
        
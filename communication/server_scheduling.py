# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 08:57:58 2019

@author: egu
"""

import socket
import pickle
import numpy as np
from datetime import date, datetime, timedelta
from pyomo.environ import SolverFactory
from optimizationmodels.deterministic.new_models.chargingcars_on_off import optimize_scheduling

class SchedulingServer(object):
    
    def __init__(self,host,port,timeoutThr=30):
        self.adress=(host,port)
        self.socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(timeoutThr)
        self.socket.bind(self.adress)
        self.client=None
        self.client_adress=None
                      
    def listen(self,nbofClients):
        self.socket.listen(nbofClients)
        print(datetime.now(),": listening to the connections requests")
        
    def accept(self):
        connection,adress=self.socket.accept()
        print(datetime.now(),": client at",adress,"is connected")
        self.client=connection
        self.client_adress=adress

    def receive_request(self,nbchar=1024*64):
        try:
            rec_byte=self.client.recv(nbchar)
            print(datetime.now(),": the client at adress %s:%s requested optimization for scheduling"%self.client_adress)
            self.actual_request=pickle.loads(rec_byte)
        except:
            print(datetime.now(),": no optimization request ")
           
    def optimize_schedules(self,optSolver):   
        client_dict=self.actual_request
         
        parkdata={}
        parkdata['Slots']=client_dict['Slots']
        parkdata['OptimizationHorizon']=client_dict['OptimizationHorizon']
        parkdata['TimeInterval']=client_dict['TimeInterval']
        parkdata['P_MMC_Max']=client_dict['P_MMC_Max']
        parkdata['alpha_p']=client_dict['alpha_p']
        parkdata['beta_p']=client_dict['beta_p']
        parkdata['Charging Efficiency']=client_dict['Charging Efficiency']
        parkdata['ReqID']=client_dict['ReqID']
        
        all_keys=client_dict.keys()
        prk_keys=parkdata.keys()
        car_keys=list(set(all_keys)-set(prk_keys))
        cardata=dict((car,client_dict[car]) for car in car_keys)
        
        self.actual_request_id=parkdata['ReqID']
        print(datetime.now(),": optimization function being called for the instance",parkdata['ReqID'])       
        charging_powers=optimize_scheduling(parkdata,cardata,optSolver)        
        print(datetime.now(),": optimization function performed")
        
        chargin_powers_byte=pickle.dumps(charging_powers)
        self.client.send(chargin_powers_byte)
        print(datetime.now(),": optimization result has been sent to the client")
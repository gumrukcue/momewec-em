# -*- coding: utf-8 -*-
"""
Created on Mon May 25 09:12:32 2020

@author: egu
"""

from classes.Car import RenaultZOE
from classes.Timer import Timer
from classes.CarPark import CarPark

import matplotlib.pyplot as plt
from datetime import date, time, timedelta
import time as ttime

import numpy as np
import pandas as pd
from random import randint
import shelve
import os

from pyomo.environ import SolverFactory

projectdir=os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
inputsdir =os.path.join(projectdir,'inputs')
pricefile =os.path.join(inputsdir,'elprice')

pricedata = shelve.open(pricefile)
price_series=pricedata['elprice']
pricedata.close() 

def test_case(nbcars,unb,hh,withopt,curtailed):
    
    np.random.seed(0)
    
    #Specific case inputs price_series
    year = 2020
    month = 1
    day = 6
    timer_start_hour= 12
    timer_start_min = 0
    timer_step_sec  = 60*5 #Size of the steps to be taken at each instant in the simulation (expressed in seconds)
    myTimer = Timer(year, month, day, timer_start_hour, timer_start_min, timer_step_sec)
        
    mmc_cell_nb_per_arm  = 50
    mmc_cell_max_power   = 22
    mmc_system_max_power = 6*mmc_cell_nb_per_arm*mmc_cell_max_power
    myPark = CarPark(myTimer, mmc_cell_nb_per_arm, mmc_system_max_power, mmc_cell_max_power)
        
    myPark.load_price(price_series,timedelta(minutes=5))
           
    sim_start   = myTimer.now                      #Hour when the simulation is starting
    sim_end     = myTimer.now+timedelta(hours=hh)   #Hour when the simulation should finish
    sim_step    = myTimer.dT
    #sim_step_min= int(myTimer.dT/timedelta(minutes=1))
    sim_step_no = int((sim_end-sim_start)/sim_step)
    sim_allsteps= [sim_start+t*sim_step for t in range(sim_step_no+1)]
        
    #Departure times are assigned to the cars at the moment of arrival
    park_enter_leave=pd.DataFrame(columns=['Object','Arrival','SoC','Departure','Desired SoC'])
       
    opt_solver  =SolverFactory("cplex")
    opt_step   =sim_step
    opt_horizon=timedelta(hours=3)
    alpha =mmc_cell_max_power*mmc_cell_nb_per_arm*2*unb
    beta  =mmc_cell_max_power*mmc_cell_nb_per_arm*1*unb
     
    #Specifcy the fleet
    free_carList = []
    fleet={}
    for n in range(1,nbcars+1):
        iD='ev'+"{:02d}".format(n)
        fleet[iD]=RenaultZOE(myTimer,iD)
        ini_soc    =np.random.randint(20,50)/100
        fin_soc    =1.0
        dod        =fin_soc-ini_soc
        fleet[iD].soc[sim_start]=ini_soc
        free_carList.append(fleet[iD])
        
        steps2full =int(dod*fleet[iD].bCapacity/(mmc_cell_max_power*sim_step.seconds))
        #print(steps2full)
        
        arrival_step  =np.random.randint(0,sim_step_no-steps2full)
        departure_step=arrival_step+steps2full
        park_enter_leave.loc[iD]={'Object':fleet[iD],'Arrival':sim_start+arrival_step*sim_step,
                            'SoC':ini_soc,'Departure':sim_start+departure_step*sim_step,
                            'Desired SoC':fin_soc}
                        
    #----------------------------------------------- Simulation -----------------------------------------------#
    for shifted_step in range(sim_step_no+1):
        
        if shifted_step!=0:
            myPark.update_timer()
        
        sim_ts=sim_start+shifted_step*sim_step
        #print("Simulation step",sim_ts)
    
        #Loop through the list of cars to leave the park at this time step
        park_leaving_cars =park_enter_leave[park_enter_leave['Departure']==sim_ts].index
        park_entering_cars=park_enter_leave[park_enter_leave['Arrival']==sim_ts].index
        
        for car_id in park_leaving_cars:
            car=park_enter_leave['Object'][car_id]
            myPark.remove_car(sim_ts,car)
            free_carList.append(car)
            
        for car_id in park_entering_cars:
            car=park_enter_leave['Object'][car_id]
            free_carList.remove(car)
            enteringCars={}
            deptime=park_enter_leave['Departure'][car_id]
            finsoc =park_enter_leave['Desired SoC'][car_id]
            enteringCars[car]={'Estimated departure':deptime,'Desired SoC':finsoc}
            myPark.enter_cars(sim_ts,enteringCars)
            
            if withopt==0:
                j,k,n = myPark.simple_allocation(sim_ts,opt_step,opt_horizon,car,opt_solver)
            if withopt==1:
                j,k,n = myPark.optimized_allocation(sim_ts,opt_step,opt_horizon,car,opt_solver)
            myPark.connect_car(sim_ts,car,j,k,n)

        if curtailed==0:
            pref=myPark.get_cell_references_for_unlimited_unbalance(sim_ts,sim_step)
        if curtailed==1:
            pref=myPark.get_cell_references_for_limited_unbalance(sim_ts,sim_step,alpha,beta)
        myPark.implement_power_references(sim_ts,pref)

    
        #Idle the cars away      
        for car in free_carList:
            car.idle(myTimer.now)  
        myPark.save_power_data(sim_ts)
        
    armpow,phspow,armcar,phscar=myPark.simulation_summary()
    
    return myPark.host_dataset,armpow,phspow,armcar,phscar,park_enter_leave

for carno in [200]:#,36,42,48]:
    for unb in [0.05]:
        for h in [4]:#,4]:
            #print("Simple non-curtailed")
            #f00,armpow00,phspow00,armcar00,phscar00,scenario00=test_case(carno,unb,h,0,0)
            #print("Smart non-curtailed")
            #f10,armpow10,phspow10,armcar10,phscar10,scenario10=test_case(carno,unb,h,1,0)
            print("Simple curtailed")
            f01,armpow01,phspow01,armcar01,phscar01,scenario01=test_case(carno,unb,h,0,1)
            #print("Smart curtailed")
            #f11,armpow11,phspow11,armcar11,phscar11,scenario11=test_case(carno,unb,h,1,1)


##%%
#figure, axes = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#axes[0,0].set_title('Phase 1')
#axes[0,1].set_title('Phase 2')
#axes[0,2].set_title('Phase 3')
#axes[1,0].set_xlabel('Time')
#axes[1,1].set_xlabel('Time')
#axes[1,2].set_xlabel('Time')
#axes[0,0].set_ylabel('Upper arm')
#axes[1,0].set_ylabel('Lower arm')
#figure.suptitle('Number of cars in each arm')
#
#armcar={}
#armcar[1,1]=pd.DataFrame()
#armcar[1,1]['Simple']=armcar00[1,1]
#armcar[1,1]['Optimized']=armcar10[1,1]
#armcar[1,2]=pd.DataFrame()
#armcar[1,2]['Simple']=armcar00[1,1]
#armcar[1,2]['Optimized']=armcar10[1,1]
#armcar[2,1]=pd.DataFrame()
#armcar[2,1]['Simple']=armcar00[2,1]
#armcar[2,1]['Optimized']=armcar10[2,1]
#armcar[2,2]=pd.DataFrame()
#armcar[2,2]['Simple']=armcar00[2,2]
#armcar[2,2]['Optimized']=armcar10[2,2]
#armcar[3,1]=pd.DataFrame()
#armcar[3,1]['Simple']=armcar00[3,1]
#armcar[3,1]['Optimized']=armcar10[3,1]
#armcar[3,2]=pd.DataFrame()
#armcar[3,2]['Simple']=armcar00[3,2]
#armcar[3,2]['Optimized']=armcar10[3,2]
#
#armcar[1,1].plot(ax=axes[0,0])
#armcar[1,2].plot(ax=axes[1,0])
#armcar[2,1].plot(ax=axes[0,1])
#armcar[2,2].plot(ax=axes[1,1])
#armcar[3,1].plot(ax=axes[0,2])
#armcar[3,2].plot(ax=axes[1,2])
#
#plt.show()
#
##%%
#figure1, axes1 = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#axes1[0,0].set_title('Phase 1')
#axes1[0,1].set_title('Phase 2')
#axes1[0,2].set_title('Phase 3')
#axes1[1,0].set_xlabel('Time')
#axes1[1,1].set_xlabel('Time')
#axes1[1,2].set_xlabel('Time')
#axes1[0,0].set_ylabel('Upper arm')
#axes1[1,0].set_ylabel('Lower arm')
#figure1.suptitle('Arm powers without suspension')
#
#armpow={}
#armpow[1,1]=pd.DataFrame()
#armpow[1,1]['Simple']=armpow00[1,1]
#armpow[1,1]['Optimized']=armpow10[1,1]
#armpow[1,2]=pd.DataFrame()
#armpow[1,2]['Simple']=armpow00[1,1]
#armpow[1,2]['Optimized']=armpow10[1,1]
#armpow[2,1]=pd.DataFrame()
#armpow[2,1]['Simple']=armpow00[2,1]
#armpow[2,1]['Optimized']=armpow10[2,1]
#armpow[2,2]=pd.DataFrame()
#armpow[2,2]['Simple']=armpow00[2,2]
#armpow[2,2]['Optimized']=armpow10[2,2]
#armpow[3,1]=pd.DataFrame()
#armpow[3,1]['Simple']=armpow00[3,1]
#armpow[3,1]['Optimized']=armpow10[3,1]
#armpow[3,2]=pd.DataFrame()
#armpow[3,2]['Simple']=armpow00[3,2]
#armpow[3,2]['Optimized']=armpow10[3,2]
#
#armpow[1,1].plot(ax=axes1[0,0])
#armpow[1,2].plot(ax=axes1[1,0])
#armpow[2,1].plot(ax=axes1[0,1])
#armpow[2,2].plot(ax=axes1[1,1])
#armpow[3,1].plot(ax=axes1[0,2])
#armpow[3,2].plot(ax=axes1[1,2])
#
#plt.show()
#
##%%
#figure2, axes2 = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#axes2[0,0].set_title('Phase 1')
#axes2[0,1].set_title('Phase 2')
#axes2[0,2].set_title('Phase 3')
#axes2[1,0].set_xlabel('Time')
#axes2[1,1].set_xlabel('Time')
#axes2[1,2].set_xlabel('Time')
#axes2[0,0].set_ylabel('Upper arm')
#axes2[1,0].set_ylabel('Lower arm')
#figure2.suptitle('Arm powers with suspension')
#
#carmpow={}
#carmpow[1,1]=pd.DataFrame()
#carmpow[1,1]['Simple']=armpow01[1,1]
#carmpow[1,1]['Optimized']=armpow11[1,1]
#carmpow[1,2]=pd.DataFrame()
#carmpow[1,2]['Simple']=armpow01[1,1]
#carmpow[1,2]['Optimized']=armpow11[1,1]
#carmpow[2,1]=pd.DataFrame()
#carmpow[2,1]['Simple']=armpow01[2,1]
#carmpow[2,1]['Optimized']=armpow11[2,1]
#carmpow[2,2]=pd.DataFrame()
#carmpow[2,2]['Simple']=armpow01[2,2]
#carmpow[2,2]['Optimized']=armpow11[2,2]
#carmpow[3,1]=pd.DataFrame()
#carmpow[3,1]['Simple']=armpow01[3,1]
#carmpow[3,1]['Optimized']=armpow11[3,1]
#carmpow[3,2]=pd.DataFrame()
#carmpow[3,2]['Simple']=armpow01[3,2]
#carmpow[3,2]['Optimized']=armpow11[3,2]
#
#carmpow[1,1].plot(ax=axes2[0,0])
#carmpow[1,2].plot(ax=axes2[1,0])
#carmpow[2,1].plot(ax=axes2[0,1])
#carmpow[2,2].plot(ax=axes2[1,1])
#carmpow[3,1].plot(ax=axes2[0,2])
#carmpow[3,2].plot(ax=axes2[1,2])
#
#plt.show()
#
#
##%%
#figure3, axes3 = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#axes3[0,0].set_title('Phase 1')
#axes3[0,1].set_title('Phase 2')
#axes3[0,2].set_title('Phase 3')
#axes3[1,0].set_xlabel('Time')
#axes3[1,1].set_xlabel('Time')
#axes3[1,2].set_xlabel('Time')
#axes3[0,0].set_ylabel('Upper arm')
#axes3[1,0].set_ylabel('Lower arm')
#figure3.suptitle('Arm power reduction')
#
#comp={}
#comp[1,1]=pd.DataFrame()
#comp[1,1]['Simple']    =armpow00[1,1]-armpow01[1,1]
#comp[1,1]['Optimized']=armpow10[1,1]-armpow11[1,1]
#comp[1,2]=pd.DataFrame()
#comp[1,2]['Simple']    =armpow00[1,1]-armpow01[1,1]
#comp[1,2]['Optimized']=armpow10[1,1]-armpow11[1,1]
#comp[2,1]=pd.DataFrame()
#comp[2,1]['Simple']    =armpow00[2,1]-armpow01[2,1]
#comp[2,1]['Optimized']=armpow10[2,1]-armpow11[2,1]
#comp[2,2]=pd.DataFrame()
#comp[2,2]['Simple']    =armpow00[2,2]-armpow01[2,2]
#comp[2,2]['Optimized']=armpow10[2,2]-armpow11[2,2]
#comp[3,1]=pd.DataFrame()
#comp[3,1]['Simple']    =armpow00[3,1]-armpow01[3,1]
#comp[3,1]['Optimized']=armpow10[3,1]-armpow11[3,1]
#comp[3,2]=pd.DataFrame()
#comp[3,2]['Simple']    =armpow00[3,2]-armpow01[3,2]
#comp[3,2]['Optimized']=armpow10[3,2]-armpow11[3,2]
#
#comp[1,1].plot(ax=axes3[0,0])
#comp[1,2].plot(ax=axes3[1,0])
#comp[2,1].plot(ax=axes3[0,1])
#comp[2,2].plot(ax=axes3[1,1])
#comp[3,1].plot(ax=axes3[0,2])
#comp[3,2].plot(ax=axes3[1,2])
#
#plt.show()
#
#
##%%
#figure4, axes4 = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#axes4[0,0].set_title('Phase 1')
#axes4[0,1].set_title('Phase 2')
#axes4[0,2].set_title('Phase 3')
#axes4[1,0].set_xlabel('Time')
#axes4[1,1].set_xlabel('Time')
#axes4[1,2].set_xlabel('Time')
#axes4[0,0].set_ylabel('Upper arm')
#axes4[1,0].set_ylabel('Lower arm')
##figure4.suptitle('Cumulative curtailment')
#
#
#cumu_cur={}
#cumu_cur[1,1]=pd.DataFrame()
#cumu_cur[1,1]['Simple']    =(armpow00[1,1]-armpow01[1,1]).cumsum()/12
#cumu_cur[1,1]['Optimized']=(armpow10[1,1]-armpow11[1,1]).cumsum()/12
#cumu_cur[1,2]=pd.DataFrame()
#cumu_cur[1,2]['Simple']    =(armpow00[1,1]-armpow01[1,1]).cumsum()/12
#cumu_cur[1,2]['Optimized']=(armpow10[1,1]-armpow11[1,1]).cumsum()/12
#cumu_cur[2,1]=pd.DataFrame()
#cumu_cur[2,1]['Simple']    =(armpow00[2,1]-armpow01[2,1]).cumsum()/12
#cumu_cur[2,1]['Optimized']=(armpow10[2,1]-armpow11[2,1]).cumsum()/12
#cumu_cur[2,2]=pd.DataFrame()
#cumu_cur[2,2]['Simple']    =(armpow00[2,2]-armpow01[2,2]).cumsum()/12
#cumu_cur[2,2]['Optimized']=(armpow10[2,2]-armpow11[2,2]).cumsum()/12
#cumu_cur[3,1]=pd.DataFrame()
#cumu_cur[3,1]['Simple']    =(armpow00[3,1]-armpow01[3,1]).cumsum()/12
#cumu_cur[3,1]['Optimized']=(armpow10[3,1]-armpow11[3,1]).cumsum()/12
#cumu_cur[3,2]=pd.DataFrame()
#cumu_cur[3,2]['Simple']    =(armpow00[3,2]-armpow01[3,2]).cumsum()/12
#cumu_cur[3,2]['Optimized']=(armpow10[3,2]-armpow11[3,2]).cumsum()/12
#
#cumu_cur[1,1].plot(ax=axes4[0,0])
#cumu_cur[1,2].plot(ax=axes4[1,0])
#cumu_cur[2,1].plot(ax=axes4[0,1])
#cumu_cur[2,2].plot(ax=axes4[1,1])
#cumu_cur[3,1].plot(ax=axes4[0,2])
#cumu_cur[3,2].plot(ax=axes4[1,2])
#
#plt.show()
#
##%%
#figure5, axes5 = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#axes5[0,0].set_title('Phase 1')
#axes5[0,1].set_title('Phase 2')
#axes5[0,2].set_title('Phase 3')
#axes5[1,0].set_xlabel('Time')
#axes5[1,1].set_xlabel('Time')
#axes5[1,2].set_xlabel('Time')
#axes5[0,0].set_ylabel('Upper arm')
#axes5[1,0].set_ylabel('Lower arm')
##figure5.suptitle('Supplied energy')
#axes5[0,0].legend().set_visible(False)
#axes5[0,1].legend().set_visible(False)
#axes5[1,0].legend().set_visible(False)
#axes5[1,1].legend().set_visible(False)
#axes5[1,2].legend().set_visible(False)
#
#cumu={}
#cumu[1,1]=pd.DataFrame()
#cumu[1,1]['Simple']    =(armpow01[1,1]).cumsum()/12
#cumu[1,1]['Optimized']=(armpow11[1,1]).cumsum()/12
#cumu[1,2]=pd.DataFrame()
#cumu[1,2]['Simple']    =(armpow01[1,1]).cumsum()/12
#cumu[1,2]['Optimized']=(armpow11[1,1]).cumsum()/12
#cumu[2,1]=pd.DataFrame()
#cumu[2,1]['Simple']    =(armpow01[2,1]).cumsum()/12
#cumu[2,1]['Optimized']=(armpow11[2,1]).cumsum()/12
#cumu[2,2]=pd.DataFrame()
#cumu[2,2]['Simple']    =(armpow01[2,2]).cumsum()/12
#cumu[2,2]['Optimized']=(armpow11[2,2]).cumsum()/12
#cumu[3,1]=pd.DataFrame()
#cumu[3,1]['Simple']    =(armpow01[3,1]).cumsum()/12
#cumu[3,1]['Optimized']=(armpow11[3,1]).cumsum()/12
#cumu[3,2]=pd.DataFrame()
#cumu[3,2]['Simple']    =(armpow01[3,2]).cumsum()/12
#cumu[3,2]['Optimized']=(armpow11[3,2]).cumsum()/12
#
#cumu[1,1].plot(ax=axes5[0,0])
#cumu[1,2].plot(ax=axes5[1,0])
#cumu[2,1].plot(ax=axes5[0,1])
#cumu[2,2].plot(ax=axes5[1,1])
#cumu[3,1].plot(ax=axes5[0,2])
#cumu[3,2].plot(ax=axes5[1,2])
#
#plt.show()
#
##%%
#figure6, axes6 = plt.subplots(figsize=(8, 6),nrows=2, ncols=3, sharey=True, sharex=True,constrained_layout=True)
#
#betal=unb*22*50
#alpal=2*unb*22*50
#vertlim=dict([(t,betal) for t in armpow00.index])
#horilim=dict([(t,alpal) for t in armpow00.index])
#
#vertunb={}
#vertunb[1,1]=pd.DataFrame()
#vertunb[1,1]['Simple']   =abs(armpow00[1,1]-armpow00[1,1])
#vertunb[1,1]['Optimized']=abs(armpow10[1,1]-armpow10[1,1])
#vertunb[1,1]['Limit']    =pd.Series(vertlim)
#vertunb[1,2]=pd.DataFrame()
#vertunb[1,2]['Simple']   =abs(armpow00[2,1]-armpow00[2,2])
#vertunb[1,2]['Optimized']=abs(armpow10[2,1]-armpow10[2,2])
#vertunb[1,2]['Limit']    =pd.Series(vertlim)
#vertunb[1,3]=pd.DataFrame()
#vertunb[1,3]['Simple']   =abs(armpow00[3,1]-armpow00[3,2])
#vertunb[1,3]['Optimized']=abs(armpow10[3,1]-armpow10[3,2])
#vertunb[1,3]['Limit']    =pd.Series(vertlim)
#
#horiunb={}
#horiunb[2,1]=pd.DataFrame()
#horiunb[2,1]['Simple']   =abs(phspow00['Phase 1']-phspow00['Phase 2'])
#horiunb[2,1]['Optimized']=abs(phspow10['Phase 1']-phspow10['Phase 2'])
#horiunb[2,1]['Limit']    =pd.Series(horilim)
#horiunb[2,2]=pd.DataFrame()
#horiunb[2,2]['Simple']   =abs(phspow00['Phase 1']-phspow00['Phase 3'])
#horiunb[2,2]['Optimized']=abs(phspow10['Phase 1']-phspow10['Phase 3'])
#horiunb[2,2]['Limit']    =pd.Series(horilim)
#horiunb[2,3]=pd.DataFrame()
#horiunb[2,3]['Simple']   =abs(phspow00['Phase 2']-phspow00['Phase 3'])
#horiunb[2,3]['Optimized']=abs(phspow10['Phase 2']-phspow10['Phase 3'])
#horiunb[2,3]['Limit']    =pd.Series(horilim)
#
#vertunb[1,1].plot(ax=axes6[0,0])
#vertunb[1,2].plot(ax=axes6[0,1])
#vertunb[1,3].plot(ax=axes6[0,2])
#horiunb[2,1].plot(ax=axes6[1,0])
#horiunb[2,2].plot(ax=axes6[1,1])
#horiunb[2,3].plot(ax=axes6[1,2])
#axes6[0,0].legend().set_visible(False)
#axes6[0,1].legend().set_visible(False)
#axes6[1,0].legend().set_visible(False)
#axes6[1,1].legend().set_visible(False)
#axes6[1,2].legend().set_visible(False)
#
#axes6[0,0].set_title('Phase 1')
#axes6[0,1].set_title('Phase 2')
#axes6[0,2].set_title('Phase 3')
#axes6[1,0].set_title('Phase 1-2')
#axes6[1,1].set_title('Phase 1-3')
#axes6[1,2].set_title('Phase 2-3')
#
#axes6[0,0].set_ylabel('Vertical unbalance')
#axes6[1,0].set_ylabel('Horizontal unbalance')
#axes6[1,0].set_xlabel('Time')
#axes6[1,1].set_xlabel('Time')
#axes6[1,2].set_xlabel('Time')
#
#
#plt.show()
#
#
##%%
#compare=pd.DataFrame()
#compare['Simple']   =f01['Leave SOC']
#compare['Optimized']=f11['Leave SOC']
#compare['Delta']    =compare['Optimized']-compare['Simple']
#
#
#for de in [-10,-5,-1]:
#    clustr='<'+str(de)+'%'
#    compare[clustr]=compare['Delta']<de/100
#    print('Delta SOC',clustr,len(compare[compare[clustr]==True]))
#for de in [1,5,10]:
#    clustr='>'+str(de)+'%'
#    compare[clustr]=compare['Delta']>de/100
#    print('Delta SOC',clustr,len(compare[compare[clustr]==True]))
#
#print("Total supply unconstrained dummy",f00['Charged Energy [kWh]'].sum())
#print("Total supply unconstrained smart",f10['Charged Energy [kWh]'].sum())
#print("Total supply dummy",f01['Charged Energy [kWh]'].sum())
#print("Total supply smart",f11['Charged Energy [kWh]'].sum())
#
#
